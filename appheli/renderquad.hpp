#pragma once

#include "engine/shaderprogram.hpp"
#include "engine/uniform.hpp"

#include "chassis/quad.hpp"

#include <glm/mat4x4.hpp>

class Wall;
class Camera;
class TBB;

class RenderQuad {
private:
  ShaderProgram shader_program;
  Uniform uniform_matrix;
  Uniform uniform_color;
  Quad quad;

public:
	static const glm::vec4 defaultColor;
  RenderQuad();
  ~RenderQuad() = default;

  RenderQuad(const RenderQuad& copy) = delete;
  RenderQuad& operator=(const RenderQuad& copy) = delete;

  RenderQuad(RenderQuad&& move) = default;
  RenderQuad& operator=(RenderQuad&& move) = default;

  void preDraw(const glm::vec4& color = defaultColor);
  void draw(const glm::mat4& matrix);
  void draw(const Wall& wall, const Camera& camera);
  void draw(const TBB& bounds, const Camera& camera);
  void draw(const TBB& bounds);
};
