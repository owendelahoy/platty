#include "renderquad.hpp"

#include "wall.hpp"

#include "engine/file.hpp"
#include "chassis/tbb.hpp"
#include "chassis/camera.hpp"

const glm::vec4 RenderQuad::defaultColor(1.0f,1.0f,1.0f,0.25f);

RenderQuad::RenderQuad() :
shader_program(File("shaders/matrix.vs"), File("shaders/color.fs")),
uniform_matrix(shader_program.getUniform("matrix")),
uniform_color(shader_program.getUniform("color")),
quad(shader_program.getAttribute("vertex")) {
  
}

void RenderQuad::preDraw(const glm::vec4& color) {
	quad.preDraw();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	shader_program.use();
	uniform_color.vec4(color);
}

void RenderQuad::draw(const glm::mat4& matrix) {
	uniform_matrix.matrix4(matrix);
	quad.draw();
}

void RenderQuad::draw(const Wall& wall, const Camera& camera) {
  draw(camera.matrix() * wall.bounds.matrix());
}

void RenderQuad::draw(const TBB& bounds, const Camera& camera) {
	draw(camera.matrix() * bounds.matrix());
}

void RenderQuad::draw(const TBB& bounds) {
	draw(bounds.matrix());
}
