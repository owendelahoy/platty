#pragma once

#include <glm/vec2.hpp>

class UI;
class CameraOrtho;
class Level;

class Heli {
public:
  glm::vec2 pos, vel;
  Heli();
  ~Heli() = default;

  Heli(const Heli& copy) = default;
  Heli& operator=(const Heli& copy) = default;
  Heli(Heli&& move) = default;
  Heli& operator=(Heli&& move) = default;

  void step(bool heliAccel, bool& paused, const Level& level);
  void die(bool& paused);
};

