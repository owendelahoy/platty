#version 100
precision highp float;

varying vec2 texel_coord;
uniform sampler2D sprite;

void main() {
  gl_FragColor = texture2D(sprite, texel_coord);
}
