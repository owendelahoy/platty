class Heli;
class TBB;

class UI {
public:
	float progress;
	float mostProgress;
	
	UI();
	~UI() = default;

	UI(const UI& copy) = default;
	UI& operator=(const UI& copy) = default;
	UI(UI&& move) = default;
	UI& operator=(UI&& move) = default;

	void setProgress(const Heli& heli, float range);
	TBB progressTBB();
	TBB mostProgressTBB();
protected:
	static TBB makeTBB(float length);
};