#include "renderheli.hpp"

#include "heli.hpp"

#include "engine/file.hpp"

#include "chassis/camera.hpp"
#include "chassis/tbb.hpp"

#include <cmath>

RenderHeli::RenderHeli() :
shader_program(File("shaders/heli.vs"), File("shaders/heli.fs")),
sprite(shader_program.getAttribute("vertex"), shader_program.getAttribute("texel")),
texture(),
uniform_matrix(shader_program.getUniform("matrix")),
uniform_camera(shader_program.getUniform("camera")),
uniform_texture(shader_program.getUniform("sprite")),
matrix(1.0f) {
  texture.test();
  uniform_texture.texture(texture);
}

void RenderHeli::preDraw(const Camera& camera) {
	sprite.preDraw();
	shader_program.use();
	uniform_camera.matrix4(camera.matrix());
}

void RenderHeli::draw(const Heli& heli) {
  float angle = std::atan2(-heli.vel.y, heli.vel.x) + std::atan2(1.0f,1.0f);
  glm::mat4 rotate(1.0f);
  rotate[0][0] = rotate[1][1] = std::cos(angle);
  rotate[1][0] = std::sin(angle);
  rotate[0][1] = -rotate[1][0];
  
  matrix[0][0] = 32.0f;
  matrix[1][1] = 32.0f;
  matrix[3][0] = heli.pos.x;// * 32.0f;
  matrix[3][1] = heli.pos.y;// * 32.0f;
  uniform_matrix.matrix4(matrix * rotate);
  sprite.draw();
}


void RenderHeli::draw(const TBB& bounds) {
	uniform_matrix.matrix4(bounds.matrix());
	sprite.draw();
}
