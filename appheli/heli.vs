#version 100

attribute vec2 vertex;
attribute vec2 texel;
varying vec2 texel_coord;
uniform mat4 matrix;
uniform mat4 camera;

void main() {
  texel_coord = texel;
  gl_Position = camera * matrix * vec4(vertex, 0.0, 1.0);
}
