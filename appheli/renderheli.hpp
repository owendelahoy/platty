#pragma once

#include "engine/shaderprogram.hpp"
#include "engine/uniform.hpp"
#include "engine/texture.hpp"

#include "chassis/sprite.hpp"

#include <glm/mat4x4.hpp>

class Heli;
class Camera;
class TBB;

class RenderHeli {
protected:
  ShaderProgram shader_program;

  Sprite sprite; //Init dep: shader_program

  Uniform uniform_matrix;//Init dep: shader_program
  Uniform uniform_camera;//Init dep: shader_program
  Texture texture;
  Uniform uniform_texture;//Init dep: shader_program
  
  glm::mat4 matrix;
public:
  RenderHeli(); 
  ~RenderHeli() = default;

  RenderHeli(const RenderHeli& copy) = delete;
  RenderHeli& operator=(const RenderHeli& copy) = default;

  RenderHeli(RenderHeli&& move) = default;
  RenderHeli& operator=(RenderHeli&& move) = default; 
 
  void preDraw(const Camera& camera);
  void draw(const Heli& heli);
  void draw(const TBB& bounds);
};
