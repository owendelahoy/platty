#include "appheli.hpp"
#include "wall.hpp"
#include "mine.hpp"
#include "shot.hpp"

#include "engine/engine.hpp"
#include "chassis/tbb.hpp"

#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include "SDL.h"

#include <algorithm>

AppHeli::AppHeli() :
window("Heli", 800, 450),
camera(AABB2d(glm::vec2(0.0f, 0.0f), glm::vec2(-1024.0f, 1024.0f))),
renderHeli(),
renderWall(),
renderQuad(),
heli(),
level(Level::RandomLevel(9464, 100, 100)),
ui(),
print(false) {
  camera.setAspect(window, false);
  renderWall.bufferWalls(level.walls);
  std::cout << glm::to_string(camera.matrix()) << std::endl;
}

void AppHeli::event(const SDL_Event& event) {
	switch (event.type) {
	case SDL_QUIT:
		Engine::singleton().stop();
		break;
	case SDL_KEYDOWN:
		switch (event.key.keysym.sym) {
		case SDLK_ESCAPE:
			Engine::singleton().stop();
			break;
		case SDLK_p:
			print = true;
			break;
		case SDLK_z:
			heliAccel = true;
			paused = false;
			break;
		}
		break;
	case SDL_KEYUP:
		switch (event.key.keysym.sym) {
		case SDLK_z:
			heliAccel = false;
			break;
		}
		break;
	case SDL_MOUSEBUTTONUP:
		mouseUp(event.button);
		break;
	case SDL_FINGERUP:
		touchUp(event.tfinger);
		break;
	case SDL_MOUSEBUTTONDOWN:
		mouseDown(event.button);
		break;
	case SDL_FINGERDOWN:
		touchDown(event.tfinger);
		break;
	}
}

void AppHeli::mouseDown(const SDL_MouseButtonEvent& event) {
	if (event.which == 0) {
		paused = false;
		glm::vec4 pointer(event.x, event.y, 0.0f, 1.0f);
		switch (event.button) {
		case SDL_BUTTON_RIGHT:
			heliAccel = true;
			break;
		case SDL_BUTTON_LEFT:
			pointer = camera.inverse_matrix() * window.pixel2device() * pointer;
			shots.emplace_back(glm::vec2(pointer.x, pointer.y), heli.pos);
			break;
		}
	}
}

void AppHeli::mouseUp(const SDL_MouseButtonEvent& event) {
	if (event.button == SDL_BUTTON_RIGHT) {
		heliAccel = false;
	}
}

void AppHeli::touchDown(const SDL_TouchFingerEvent& event) {
	paused = false;
	glm::vec4 pointer(event.x, event.y, 0.0f, 1.0f);
#ifndef EMSCRIPTEN
	pointer.x *= window.width();
	pointer.y *= window.height();
#endif
	pointer = window.pixel2device() * pointer;
	if (pointer.x >= 2.0 / 3.0f) {
		heliAccel = true;
		heliAccelTouches.push_back(event.fingerId);
	} else {
		pointer = camera.inverse_matrix() * pointer;
		shots.emplace_back(glm::vec2(pointer.x, pointer.y), heli.pos);
	}
}

void AppHeli::touchUp(const SDL_TouchFingerEvent& event) {
	auto fingerID = [=](const SDL_TouchID& touch) {return (event.fingerId == touch); };
	auto toRemove = std::remove_if(heliAccelTouches.begin(), heliAccelTouches.end(), fingerID);
	heliAccelTouches.erase(toRemove, heliAccelTouches.end());
	if (heliAccelTouches.empty()) {
		heliAccel = false;
	}
}

void AppHeli::step() {
	if (print) {
		Engine::singleton().printNextTimings();
		print = false;
	}
	if (!paused) {
		for (auto& mine : level.mines) {
			mine.step(heli);
		}

		shots.erase(std::remove_if(shots.begin(), shots.end(), [](Shot& shot) {return shot.step();}), shots.end());
		for (const auto& shot : shots) {
			auto deadMines = std::remove_if(
				level.mines.begin(),
				level.mines.end(),
				[&](const Mine& mine) {return shot.bounds.intersects(mine.position, 32.0f); });
			level.mines.erase(deadMines, level.mines.end());
		}

		heli.step(heliAccel, paused, level);
	}
}

void AppHeli::render() {
  glClear(GL_COLOR_BUFFER_BIT);
  ui.setProgress(heli, level.width);

  camera.setCenter(glm::vec2(heli.pos.x, 0.0f), glm::vec2(0.9f, 0.0f));
  
  renderHeli.preDraw(camera);
  renderHeli.draw(heli);
  TBB tbb(glm::vec2(0.0f, 0.0f), glm::vec2(16.0f, 16.0f));
  Engine::singleton().pushTimingNote("Mines");
  for (const auto& mine : level.mines) {
	  tbb.position = mine.position;
	  renderHeli.draw(tbb);
  }
  Engine::singleton().popTimingNote();

  renderWall.preDraw(glm::vec4(0.5f, 0.5f, 1.0f, 0.6f));
  Engine::singleton().pushTimingNote("Walls");
  renderWall.draw(camera);
  Engine::singleton().popTimingNote();


  renderQuad.preDraw(RenderQuad::defaultColor);
  Engine::singleton().pushTimingNote("Shots");
  for (auto& shot : shots) {
	  renderQuad.draw(shot.bounds, camera);
  }
  Engine::singleton().popTimingNote();
  
  renderQuad.draw(ui.progressTBB());
  renderQuad.draw(ui.mostProgressTBB());
  
  tbb.axis1.x = 0.01f;
  tbb.axis2.x = tbb.axis1.y = 0.0f;
  tbb.axis2.y = Engine::singleton().lastFrameTime();
  tbb.position.x = 0.95f;
  tbb.position.y = 0.0f;
  renderQuad.draw(tbb);
  
  window.swap();
}

void AppHeli::contextLost() {
	paused = true;
}

void AppHeli::contextRestored() {
  renderHeli = RenderHeli();
  renderQuad = RenderQuad();
  renderWall = RenderWall();
}
