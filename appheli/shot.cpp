#include "shot.hpp"

#include "glm/geometric.hpp"

Shot::Shot(glm::vec2 start, glm::vec2 end) :
	bounds(end, (start - end) * 0.5f, 5.0f),
	life(60) {
	float length = glm::length(bounds.axis1);
	if (length == 0.0f) {
		bounds.axis1.x = length = 1.0f;
	}
	bounds.axis1 *= 100.0f / length;
	bounds.position += bounds.axis1;
}

bool Shot::step() {
	life--;
	bounds.position += bounds.axis1;
	return (life <= 0);
}
