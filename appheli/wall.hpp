#pragma once

#include "chassis/tbb.hpp"

class Wall {
public:
	TBB bounds;
	Wall() = default;
	Wall(const glm::vec2& position, const glm::vec2& dimension);

	void step();
};
