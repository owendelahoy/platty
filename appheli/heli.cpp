#include "heli.hpp"

#include "ui.hpp"
#include "wall.hpp"
#include "mine.hpp"
#include "level.hpp"

#include <glm/common.hpp>

#include <iostream>
#include <cmath>


Heli::Heli() :
pos(0.0f, 0.0f),
vel(20.0f, 0.0f) {
  std::cout << "Heli created" << std::endl;
}

void Heli::step(bool heliAccel, bool& paused, const Level& level) {
    if(heliAccel) {
      vel.y += 4.0f;
    } else {
      vel.y -= 4.0f;
    }

    vel.y *= 0.92f;
    pos += vel;
    
	if (std::fabs(pos.y) > 1024.0f + 64.0f) {
		die(paused);
		return;
	}

	//int i = std::floorl(pos.x + 1024.0f - 128.0f) / 1024.0f;
	for (const auto& wall : level.walls) {
		if (wall.bounds.intersects(pos, 32.0f)) {//TODO: change from rad check to TBB
			die(paused);
			return;
		}
	}

	TBB mineTbb(glm::vec2(0.0f, 0.0f), glm::vec2(16.0f, 16.0f));
	for (const auto& mine : level.mines) {
		mineTbb.position = mine.position;
		if (mineTbb.intersects(pos, 32.0f)) {//TODO: change from rad check to TBB
			die(paused);
			return;
		}
	}
}


void Heli::die(bool& paused) {
	pos.x = 0.0f;
	pos.y = 0.0f;
	vel.y = 0.0f;
	paused = true;
}