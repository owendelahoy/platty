#pragma once

#include "engine/vertexbuffer.hpp"
#include "engine/vertexarray.hpp"
#include "engine/shaderprogram.hpp"
#include "engine/uniform.hpp"

#include "glm/mat4x4.hpp"

#include <vector>

class Wall;
class Camera;

class RenderWall {
protected:
	VertexArray vertex_array;
	VertexBuffer vertex_buffer;
	//VertexBuffer index_buffer;
	ShaderProgram shader_program;
	Uniform uniform_matrix;
	Uniform uniform_color;
	GLsizei count;

public:
	RenderWall();
	~RenderWall() = default;

	RenderWall(const RenderWall& copy) = default;
	RenderWall& operator=(const RenderWall& copy) = default;
	RenderWall(RenderWall&& move) = default;
	RenderWall& operator=(RenderWall&& move) = default;

	void bufferWalls(const std::vector<Wall>& walls);
	void preDraw(const glm::vec4& color);
	void draw(const glm::mat4& matrix);
	void draw(const Camera& camera);
};