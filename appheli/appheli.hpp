#pragma once

#include "renderheli.hpp"
#include "renderwall.hpp"
#include "renderquad.hpp"

#include "heli.hpp"
#include "level.hpp"
#include "ui.hpp"

#include "chassis/cameraortho.hpp"

#include "engine/app.hpp"
#include "engine/window.hpp"

#include "SDL_events.h"
#include "glm/vec4.hpp"

#include <vector>
#include <algorithm>

class Shot;

enum class TouchAction {
	move,
	shoot
};

class Touch {
	TouchAction action;
	SDL_TouchID id;
	glm::vec2 point;
};

class AppHeli : public App {
protected:
	Window window;
	CameraOrtho camera;
	RenderHeli renderHeli;
	RenderWall renderWall;
	RenderQuad renderQuad;

	Heli heli;
	std::vector<Shot> shots;
	Level level;
	UI ui;

	std::vector<SDL_TouchID> heliAccelTouches;
	bool heliAccel;
	bool paused;
	bool print;
public:
	AppHeli();
	~AppHeli() = default;

	void event(const SDL_Event& event) override;

	void mouseDown(const SDL_MouseButtonEvent& event);
	void mouseUp(const SDL_MouseButtonEvent& event);

	void touchDown(const SDL_TouchFingerEvent& event);
	void touchUp(const SDL_TouchFingerEvent& event);

	void step() override;
	void render() override;

	void contextLost() override;
	void contextRestored() override;
};
