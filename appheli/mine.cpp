#include "mine.hpp"

#include "appheli/heli.hpp"
#include "glm/geometric.hpp"

#include <iostream>

Mine::Mine(const glm::vec2& position) :
	position(position),
	velocity(0.0f, 0.0f) {

}

void Mine::step(const Heli& heli) {
	glm::vec2 delta = heli.pos - position;
	float length = glm::length(delta);
	if(length < 1024.0f) {
		velocity += 5.0f * delta / (length + 1.0f);
	}
	velocity *= 0.75;
	position += velocity;
}