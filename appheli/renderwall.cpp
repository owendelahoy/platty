#include "renderwall.hpp"

#include "wall.hpp"

#include "engine/file.hpp"
#include "engine/shadervar.hpp"
#include "chassis/camera.hpp"

#include "glm/vec2.hpp"

RenderWall::RenderWall() :
	vertex_array(),
	vertex_buffer(),
	//index_buffer(),
	shader_program(File("shaders/matrix.vs"), File("shaders/color.fs")),
	uniform_matrix(shader_program.getUniform("matrix")),
	uniform_color(shader_program.getUniform("color")) {
	//glDisable(GL_CULL_FACE);
}

void RenderWall::bufferWalls(const std::vector<Wall>& walls) {
	std::vector<glm::vec2> wall_vertexes(0);
	std::vector<GLushort> wall_indexes(0);

	//int index = 0;
	for (const auto& wall : walls) {
		wall_vertexes.push_back(wall.bounds.position - wall.bounds.axis1 - wall.bounds.axis2);//0
		wall_vertexes.push_back(wall.bounds.position + wall.bounds.axis1 - wall.bounds.axis2);//1
		wall_vertexes.push_back(wall.bounds.position - wall.bounds.axis1 + wall.bounds.axis2);//2

		wall_vertexes.push_back(wall.bounds.position - wall.bounds.axis1 + wall.bounds.axis2);//2
		wall_vertexes.push_back(wall.bounds.position + wall.bounds.axis1 + wall.bounds.axis2);//3
		wall_vertexes.push_back(wall.bounds.position + wall.bounds.axis1 - wall.bounds.axis2);//1

		/*
		wall_indexes.push_back(index + 0);
		wall_indexes.push_back(index + 1);
		wall_indexes.push_back(index + 2);

		wall_indexes.push_back(index + 2);
		wall_indexes.push_back(index + 3);
		wall_indexes.push_back(index + 1);

		index += 4;
		*/
	}
	count = walls.size() * 6;

	vertex_array.bind();
	size_t size = walls.size() * 4 * sizeof(glm::vec2);
	vertex_buffer.vertexData(wall_vertexes);
	//size = walls.size() * 6 * sizeof(GLushort);
	//index_buffer.indexData(size, wall_indexes.data());
	vertex_array.attrib(shader_program.getAttribute("vertex")->index, 2);
}

void RenderWall::preDraw(const glm::vec4& color) {
	vertex_array.bind();
	shader_program.use();
	uniform_color.vec4(color);
}

void RenderWall::draw(const glm::mat4& matrix) {
	uniform_matrix.matrix4(matrix);
	vertex_array.draw(GL_TRIANGLES, 0, count);
}

void RenderWall::draw(const Camera& camera) {
	draw(camera.matrix());
}