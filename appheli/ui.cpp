#include "ui.hpp"
#include "engine/window.hpp"

#include "heli.hpp"
#include "chassis/tbb.hpp"
#include "glm/vec4.hpp"

UI::UI() :
	progress(0.0f),
	mostProgress(0.0f) {
}

void UI::setProgress(const Heli& heli, float range) {
	progress = heli.pos.x / range;
	if (progress > mostProgress) {
		mostProgress = progress;
	}
}

TBB UI::progressTBB() {
	return makeTBB(progress);
}

TBB UI::mostProgressTBB() {
	return makeTBB(mostProgress);
}

TBB UI::makeTBB(float length) {
	length *= 0.9f;
	return TBB(glm::vec2(0.9f - length, -0.95f), glm::vec2(length, 0.0125f));
}