#pragma once

#include "chassis/tbb.hpp"

class Shot {
public:
	TBB bounds;
	int life;

	Shot(glm::vec2 start, glm::vec2 end);
	~Shot() = default;

	Shot(const Shot& copy) = default;
	Shot& operator=(const Shot& copy) = default;

	Shot(Shot&& move) = default;
	Shot& operator=(Shot&& move) = default;

	bool step();
};
