#include "engine/engine.hpp"

#include "appheli.hpp"
#include "wall.hpp"
#include "mine.hpp"
#include "shot.hpp"

Engine& engine = Engine::singleton();

std::unique_ptr<AppHeli> app; //TODO: move to stack, when emscripten bug is fixed

int main() {
  std::cout << "Begin main" << std::endl;
  app = std::make_unique<AppHeli>();
  engine.enableMultisample();
  engine.set_app(*app);
  engine.go();
  return 0;
}
