#include "level.hpp"

#include "wall.hpp"
#include "mine.hpp"

#include <random>
#include <iostream>

Level::Level() :
	width(0.0f) {
}
Level::Level(float width) :
	width(width) {
}

Level Level::RandomLevel(int seed, int wallCount, int mineCount) {
	Level level(wallCount * 1024.0f);
	std::mt19937 gen(seed);

	std::uniform_real_distribution<float> rand_y(-768.0f, 768.0f);
	std::uniform_real_distribution<float> rand_x(0.0f, 128.0f);
	std::uniform_real_distribution<float> rand_h(128.0f, 512.0f);
	auto position = glm::vec2(0.0f, 0.0f);
	auto size = glm::vec2(32.0f, 32.0f);
	for (int i = 0; i < wallCount; i++) {
		position.x = 1024.0f * i + rand_x(gen);
		position.y = rand_y(gen);
		size.y = rand_h(gen);
		level.walls.push_back(Wall(position, size));
	}

	std::uniform_real_distribution<float> rand_mx(2048.0f, 1024.0f * wallCount);
	std::uniform_real_distribution<float> rand_my(-1024.0f, 1024.0f);
	for (int i = 0; i < mineCount; i++) {
		position.x = rand_mx(gen);
		position.y = rand_my(gen);
		std::cout << position.x << ", " << position.y << std::endl;
		level.mines.push_back(Mine(position));
	}
	return level;
}