#pragma once
#include <vector>

class Wall;
class Mine;

class Level {
public:
	float width;
	std::vector<Wall> walls;
	std::vector<Mine> mines;

	Level();
	Level(float width);
	~Level() = default;

	Level(const Level& copy) = default;
	Level& operator=(const Level& copy) = default;
	Level(Level&& move) = default;
	Level& operator=(Level&& move) = default;

	static Level RandomLevel(int seed, int wallCount = 100, int mineCount = 40);
};

