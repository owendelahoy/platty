#pragma once

#include "glm/vec2.hpp"

class Heli;

class Mine
{
public:
	glm::vec2 position, velocity;

	Mine(const glm::vec2& position);
	~Mine() = default;

	Mine(const Mine& copy) = default;
	Mine& operator=(const Mine& copy) = default;
	Mine(Mine&& move) = default;
	Mine& operator=(Mine&& move) = default;

	void step(const Heli& heli);
};

