#include "engine/engine.hpp"

#include "appshadow.hpp"

Engine& engine = Engine::singleton();

std::unique_ptr<AppShadow> app; //TODO: move to stack, when emscripten bug is fixed

int main() {
  std::cout << "Begin main" << std::endl;
  app = std::make_unique<AppShadow>();
  engine.enableMultisample();
  engine.set_app(*app);
  engine.go();
}
