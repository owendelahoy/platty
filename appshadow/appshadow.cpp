#include "appshadow.hpp"
#include "lightdrawer.hpp"
#include "line.hpp"
#include "linedrawer.hpp"

#include "engine/engine.hpp"
#include "engine/window.hpp"


AppShadow::AppShadow() :
window("Shadows"),
light_line_r(-0.9f, -0.4f, -0.9f,  0.4f, 1.0f, 0.0f, 0.0f),
light_line_g(-0.4f,  0.9f,  0.4f,  0.9f, 0.0f, 1.0f, 0.0f),
light_line_b( 0.9f,  0.4f,  0.9f, -0.4f, 0.0f, 0.0f, 1.0f),
cast_line(0.0f, 0.4f, -0.2f, -0.3f, 0.0f, 0.0f, 0.0f),
lines({&light_line_g, &light_line_r, &light_line_g, &light_line_b, &cast_line}),
lights({&light_line_r, &light_line_g, &light_line_b}),
drawer_line(),
drawer_light() {
  std::cout << "App shadow constructed" << std::endl;
}

void AppShadow::mouseDown() {
  mouse_down = true;
  for(Line* line : lines) {
    line->select(mouse.x, mouse.y);
  }
}

void AppShadow::mouseUp() {
  mouse_down = false;
  for(Line* line : lines) {
    line->deselect();
  }
}

void AppShadow::mouseCoords(float mouseX, float mouseY) {
  mouse = window.pixel2device(mouseX, mouseY);

  for(Line* line : lines) {
    line->move(mouse.x, mouse.y);
  }
}

void AppShadow::event(const SDL_Event& event) {
  switch(event.type) {
  case SDL_QUIT:
    Engine::singleton().stop();
    break;

  case SDL_KEYDOWN:
      switch(event.key.keysym.sym) {
      case SDLK_ESCAPE:
        Engine::singleton().stop();
        break;
      }
      break;
  case SDL_MOUSEBUTTONDOWN:
    mouseCoords(event.button.x, event.button.y);
    printf("mouse down at (%f,%f)\n", mouse.x , mouse.y);
    if(event.button.button == SDL_BUTTON_LEFT) {
      mouseDown();
    }
    break;
  case SDL_MOUSEBUTTONUP:
    mouseCoords(event.button.x, event.button.y);
    printf("mouse up at (%f,%f)\n", mouse.x , mouse.y);
    if(event.button.button == SDL_BUTTON_LEFT) {
      mouseUp();
    }
    break;
  case SDL_MOUSEMOTION:
    mouseCoords(event.motion.x, event.motion.y);
    break;
  case SDL_FINGERUP:
    mouseCoords(event.tfinger.x, event.tfinger.y);
    printf("finger up at (%f,%f)(%f,%f)\n", event.tfinger.x, event.tfinger.y, mouse.x , mouse.y);
    mouseUp();
    break;
  case SDL_FINGERDOWN:
    mouseCoords(event.tfinger.x, event.tfinger.y);
    printf("finger down at (%f,%f)(%f,%f)\n", event.tfinger.x, event.tfinger.y, mouse.x , mouse.y);
    mouseDown();
    break;
  case SDL_FINGERMOTION:
    mouseCoords(event.tfinger.x, event.tfinger.y);
    break;
  }
}

void AppShadow::step() {

}

void AppShadow::render() {
  glClear(GL_COLOR_BUFFER_BIT);
  drawer_light.draw(lights, &cast_line);
  drawer_line.draw(lines);
  window.swap();
}

void AppShadow::contextLost() {

}

void AppShadow::contextRestored() {
  drawer_line.renew();
  drawer_light.renew();
}
