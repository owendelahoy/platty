#pragma once

#include "engine/shaderprogram.hpp"
#include "engine/vertexarray.hpp"
#include "engine/vertexbuffer.hpp"
#include "engine/uniform.hpp"

#include <vector>

class Line;

class LineDrawer {
protected:
  ShaderProgram shader_program;

  VertexArray vertex_array;
  VertexBuffer vertex_buffer;

  Uniform uniform_c;
  Uniform uniform_v;
  Uniform uniform_color;
  Uniform uniform_width;

  static const char*const vertex_shader_source;
  static const char*const fragment_shader_source;

  //Can't be copied due to uncopyable members
  LineDrawer(const LineDrawer& copy) = delete;
  LineDrawer& operator=(const LineDrawer& copy) = delete;

  LineDrawer(LineDrawer&& move) = default;
  LineDrawer& operator=(LineDrawer&& move) = default;

public:
  LineDrawer();
  ~LineDrawer() = default;

  void renew();

  void draw(std::vector<Line*>& lines);
};
