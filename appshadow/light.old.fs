
#version 100

precision highp float;
varying vec2 pos1;
uniform vec3 color;
vec3 color2 = vec3(0.0, 0.0, 0.0);
uniform vec2 light_c;
uniform vec2 light_v;
vec2 orth_light = vec2(light_v.y, -light_v.x);
uniform vec2 cast_c;
uniform vec2 cast_v;
vec2 orth_cast = vec2(cast_v.y, -cast_v.x);

vec2 orth(vec2 vec) {
  return vec2(vec.y, -vec.x);
}

void main() {
  vec2 delta = pos1 - light_c;
  float light_vlen2 = light_v.x*light_v.x + light_v.y*light_v.y;
  float o_length = dot(delta, orth(light_v));
  float p_length = dot(delta, light_v);
  if(p_length < 0.0) {p_length = abs(p_length);}
  else {
   p_length -= light_vlen2;
   if(p_length < 0.0) {p_length = 0.0;}
  }
  float r = (o_length*o_length + p_length*p_length)/light_vlen2;
  float light =  0.3/(0.5+r);

  color2.r = 1.0;
  vec2 pline = orth(cast_c - pos1);
  float t1 = dot(orth_light, light_c - pos1) / dot(orth_light, orth(pline));
  vec2 pline2 = orth(cast_c + cast_v - pos1);
  float t2 = dot(orth_light, light_c - pos1) / dot(orth_light, orth(pline2));

  bool wipe = false;
  bool side = (dot(pline, pos1 - cast_c - cast_v) > 0.0);
  bool light_1 = (dot(pline, pos1 - light_c) > 0.0);
  bool light_2 = (dot(pline, pos1 - light_c - light_v) > 0.0);

  if(t1 < -1.0) {
    if(light_1 != light_2) {
      float t = dot(pline, pos1 - light_c)/dot(pline, light_v);
      color2.r -= clamp((side == light_2) ? 1.0 - t : t, 0.0, 1.0);
    }
  }

  bool side2 = (dot(pline2, pos1 - cast_c) > 0.0);
  bool light_12 = (dot(pline2, pos1 - light_c) > 0.0);
  bool light_22 = (dot(pline2, pos1 - light_c - light_v) > 0.0);
  if(t2 < -1.0) {
    if(light_12 != light_22) {
      float t = dot(pline2, pos1 - light_c)/dot(pline2, light_v);
      color2.r -= clamp((side2 == light_12) ? t : 1.0 - t, 0.0, 1.0);
    }
  }

  if(light_12 == light_22 && light_22 == side2 && light_1 == light_2 && light_2 == side)
  {color2.r = 0.0;}

  if(color2.r < 0.0)
  {color2.r = 1.0+color2.r;}

  gl_FragColor = vec4(color*light*color2.r, 1.0);
}
