#version 100

attribute vec2 pos;
varying vec2 pos1;

void main() {
  pos1 = pos;
  gl_Position = vec4(pos, 0.0, 1.0);
}
