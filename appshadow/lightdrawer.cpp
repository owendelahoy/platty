#include "lightdrawer.hpp"

#include "line.hpp"

#include "engine/file.hpp"
#include "engine/shadervar.hpp"

LightDrawer::LightDrawer() :
shader_program(File("shaders/light.vs"), File("shaders/light.fs")),
uniform_light_c(shader_program.getUniform("light_c")),
uniform_light_v(shader_program.getUniform("light_v")),
uniform_cast_c(shader_program.getUniform("cast_c")),
uniform_cast_v(shader_program.getUniform("cast_v")),
uniform_color(shader_program.getUniform("color")),
vertex_buffer(),
vertex_array() {
  glEnable(GL_BLEND);
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  GLfloat quad[] = {1.0f,1.0f, 1.0f,-1.0f, -1.0f,1.0f, -1.0f,-1.0f};
  vertex_buffer.vertexData(sizeof(quad), quad, GL_STATIC_DRAW);
  vertex_array.attrib(shader_program.getAttribute("pos")->index, 2);
}

void LightDrawer::renew() {
  (*this) = LightDrawer();
}

void LightDrawer::draw(std::vector<Line*>& light_lines, Line* cast_line) {
  glClear(GL_COLOR_BUFFER_BIT);
  glBlendFunc(GL_ONE, GL_ONE);
  shader_program.use();
  for(Line* light_line : light_lines) {
    light_line->draw_values(uniform_light_c, uniform_light_v, uniform_color);
    cast_line->draw_values(uniform_cast_c, uniform_cast_v);
    vertex_array.draw(GL_TRIANGLE_STRIP,0,4);
  }
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
