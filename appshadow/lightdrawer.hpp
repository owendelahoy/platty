#pragma once

#include "engine/vertexarray.hpp"
#include "engine/vertexbuffer.hpp"
#include "engine/shaderprogram.hpp"
#include "engine/uniform.hpp"

#include <vector>

class Line;

class LightDrawer {
protected:
  ShaderProgram shader_program; 
  VertexArray vertex_array;
  VertexBuffer vertex_buffer;

  Uniform uniform_light_c;
  Uniform uniform_light_v;
  Uniform uniform_cast_c;
  Uniform uniform_cast_v;
  Uniform uniform_color;

  LightDrawer(const LightDrawer& copy) = default;
  LightDrawer& operator=(const LightDrawer& copy) = default;

  LightDrawer(LightDrawer&& move) = default;
  LightDrawer& operator=(LightDrawer&& move) = default;
  
public:
  LightDrawer();
  ~LightDrawer() = default;

  void renew();

  void draw(std::vector<Line*>& light_lines, Line* cast_line);
};
