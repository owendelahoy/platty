#include "linedrawer.hpp"

#include "line.hpp"

#include "engine/vertexshader.hpp"
#include "engine/fragmentshader.hpp"
#include "engine/shadervar.hpp"

const char*const LineDrawer::vertex_shader_source =
  "#version 100\n"
  "attribute vec2 t;"
  "uniform vec2 c;"
  "uniform vec2 v;"
  "uniform float width;"
  "vec2 vn;"
  "void main() {"
  " vn = normalize(vec2(-v.y, v.x))*width;"
  " gl_Position = vec4(v*t.x + vn*t.y + c, 0.0, 1.0);"
  "}";

const char*const LineDrawer::fragment_shader_source =
  "#version 100\n"
  "precision highp float;"
  "uniform vec3 color;"
  "void main() {"
  "   gl_FragColor = vec4(color, 1.0);"
  "}";

LineDrawer::LineDrawer() :
shader_program(VertexShader(vertex_shader_source), FragmentShader(fragment_shader_source)),
uniform_c(shader_program.getUniform("c")),
uniform_v(shader_program.getUniform("v")),
uniform_width(shader_program.getUniform("width")),
uniform_color(shader_program.getUniform("color")),
vertex_buffer(),
vertex_array()
{
  GLfloat data[] = {0.0f,0.5f, 0.0f,-0.5f, 1.0f,0.5f, 1.0f,-0.5f};
  vertex_buffer.vertexData(sizeof(data), data, GL_STATIC_DRAW);
  vertex_array.attrib(shader_program.getAttribute("t")->index, 2);
  shader_program.use();
  uniform_color.float3(1.0f,1.0f,1.0f);
  uniform_width.float1(0.01f);
}

void LineDrawer::renew() {
  (*this) = LineDrawer();
}

void LineDrawer::draw(std::vector<Line*>& lines) {
  shader_program.use();
  for(Line* line : lines) {
    line->draw_values(uniform_c, uniform_v);
    vertex_array.draw(GL_TRIANGLE_STRIP,0,4);
  }
}
