#version 100

precision highp float;
varying vec2 pos1;
uniform vec3 color;

uniform vec2 light_c;
uniform vec2 light_v;

uniform vec2 cast_c;
uniform vec2 cast_v;

vec2 orth(vec2 v) {
  return vec2(v.y, -v.x);
}

float intersection(vec2 c1, vec2 v1, vec2 c2, vec2 v2) {
  vec2 ov1 = orth(v1);
  return dot(ov1, c1-c2) / dot(ov1, v2);
}

float light(float t, vec2 c, vec2 v, vec2 p) {
  vec2 d = c - p;
  float numer = dot(v,d) + t*dot(v,v);
  vec2 vo = orth(v);
  float denom = sqrt(pow(dot(d,vo), 2.0) + dot(v,v));
  return atan(numer,denom) / denom;
}

void main() {
  vec3 color2 = vec3(0.0, 0.0, 0.0);
  vec2 lv1 = cast_c - pos1;
  vec2 lv2 = lv1 + cast_v;

  float t1,t2;
  bool side1,side2;

  float l = 1.0;

  t1 = intersection(pos1, lv1, light_c, light_v);
  side1 = (dot(orth(lv1),light_c-pos1) > 0.0);
  side2 = (dot(orth(lv1),light_c+light_v-pos1) > 0.0);
  if(side1 != side2) {
    if(intersection(light_c, light_v, pos1, lv1) > 1.0) {
      color2.r = 1.0;
      // if(dot(orth(lv1),lv2) > 0.0) {
      //   l += light(1.0, light_c, light_v, pos1)
      //   - light(t1, light_c, light_v, pos1);
      // } else {
      //   l += light(t1, light_c, light_v, pos1)
      //   - light(0.0, light_c, light_v, pos1);
      // }
    }
  }

  t2 = intersection(pos1, lv2, light_c, light_v);
  side1 = (dot(orth(lv2),light_c-pos1) > 0.0);
  side2 = (dot(orth(lv2),light_c+light_v-pos1) > 0.0);
  if(side1 != side2) {
    if(intersection(light_c, light_v, pos1, lv2) > 1.0) {
      color2.b = 1.0;
      // if(dot(orth(lv2),lv1) > 0.0) {
      //   l += light(1.0, light_c, light_v, pos1)
      //   - light(t2, light_c, light_v, pos1);
      // } else {
      //   l += light(t2, light_c, light_v, pos1)
      //   - light(0.0, light_c, light_v, pos1);
      // }
    }
  }

  // l = light(1.0, light_c, light_v, pos1) - light(0.0, light_c, light_v, pos1);

  if(l > 1.0) {
    color2 = vec3(1.0, 1.0, 1.0);
  }
  if(l < 0.0) {
    color2 = vec3(1.0, 1.0, 1.0);
  }

  gl_FragColor = vec4(color2*l, 1.0);
}
