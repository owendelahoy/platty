#include "line.hpp"

#include <glm/geometric.hpp>

#include "engine/uniform.hpp"

Line::Line(float x1, float y1, float x2, float y2, float r = 1.0f, float g = 1.0f, float b = 1.0f) :
point1(x1, y1),
point2(x2, y2),
delta(point2 - point1),
select_index(0),
select_delta(0.0f,0.0f),
color(r,g,b) {

}

Line::~Line() {

}

void Line::update_delta() {
  delta = point2 - point1;
}

void Line::select(float x, float y) {
  glm::vec2 s(x,y);

  if(glm::distance(point1,s) < 0.1f) {
    select_index = 1;
  } else {
    if(glm::distance(point2,s) < 0.1f) {
    select_index = 2;
    } else {
      glm::vec2 normal = glm::normalize(glm::vec2(delta.y, -delta.x));
      select_delta = s - point1;
      float distance = glm::dot(select_delta, normal);
      float t = glm::dot(select_delta, glm::normalize(delta)) / glm::length(delta);
      if(distance < 0.05f && distance > -0.05f && t > 0.0f && t < 1.0f) {
        select_index = 3;
      }
    }
  }
  move(x,y);
}

void Line::move(float x, float y) {
  switch(select_index) {
    case 1:
      set_point1(x,y);
      break;
    case 2:
      set_point2(x,y);
      break;
    case 3:
      point1.x = x - select_delta.x;
      point2.x = point1.x + delta.x;
      point1.y = y - select_delta.y;
      point2.y = point1.y + delta.y;
      break;
  }
}

void Line::deselect() {
  select_index = 0;
}

void Line::draw_values(Uniform c, Uniform v) const {
  c.vec2(point1);
  v.vec2(delta);
}

void Line::draw_values(Uniform c, Uniform v, Uniform color) const {
  draw_values(c,v);
  color.vec3(this->color);
}

void Line::set_point1(float x, float y) {
  point1.x = x;
  point1.y = y;
  update_delta();
}

void Line::set_point2(float x, float y) {
  point2.x = x;
  point2.y = y;
  update_delta();
}
