#pragma once

#include "engine/app.hpp"
#include "engine/window.hpp"

#include "line.hpp"
#include "linedrawer.hpp"
#include "lightdrawer.hpp"

#include <glm/vec2.hpp>
#include "SDL_events.h"

#include <vector>

class AppShadow : public App {
private:
  bool mouse_down = false;
  glm::vec2 mouse;

  Window window;

  Line light_line_r;
  Line light_line_g;
  Line light_line_b;
  Line cast_line;

  std::vector<Line*> lights;
  std::vector<Line*> lines;

  LineDrawer drawer_line;
  LightDrawer drawer_light;

  void mouseDown();
  void mouseUp();
  void mouseCoords(float mouseX, float mouseY);

public:
  AppShadow();
  ~AppShadow() = default;

  void event(const SDL_Event& event) override;
  void step() override;
  void render() override;

 void contextLost() override;
 void contextRestored() override;
};
