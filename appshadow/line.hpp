#pragma once

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

class Uniform;

class Line {
protected:
  glm::vec2 point1, point2, delta, select_delta;
  glm::vec3 color;
  int select_index;
  void update_delta();

public:
  Line(float x1, float y1, float x2, float y2, float r, float g, float b);
  ~Line();

  void draw_values(Uniform c, Uniform v) const;
  void draw_values(Uniform c, Uniform v, Uniform color) const;
  void set_point1(float x, float y);
  void set_point2(float x, float y);

  void select(float x, float y);
  void move(float x, float y);
  void deselect();
};
