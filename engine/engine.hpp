#pragma once

#include <memory>
#include <vector>
#include <chrono>
#include <iostream>

#define GLEW_STATIC
#include <GL/glew.h>

#include <SDL_events.h>

class App;
class Shader;
class ShaderProgram;
class VertexArray;
class VertexBuffer;
class VertexShader;
class Window;

struct SDL_Window;

struct TimingNote {
	TimingNote(unsigned short stack, std::string tag, std::chrono::steady_clock::time_point start) :
		stack(stack),
		tag(tag),
		start(start),
		time(-1) {
	}
	unsigned short stack;
	std::string tag;
	std::chrono::steady_clock::time_point start;
	std::chrono::steady_clock::duration time;
};


class Engine {
  friend class Window;
  #ifdef __EMSCRIPTEN__
  friend void go_emscripten();
  #endif
private:
  SDL_Window* window;
  SDL_GLContext context;
  SDL_Event sdl_event;
  bool keep_going;
  App* app;

  const std::chrono::steady_clock::duration frameTime = std::chrono::duration_cast<std::chrono::steady_clock::duration>(std::chrono::milliseconds(16));
  std::chrono::steady_clock::time_point lastTime;
  std::chrono::steady_clock::duration extraTime;
  std::chrono::steady_clock::duration deltaTime;
  
  bool printTimings;
  unsigned short noteStackSize;
  std::vector<TimingNote> notes;

  Engine();
  ~Engine();

  //Uncopyable
  Engine(Engine const&) = delete;
  void operator=(Engine const&) = delete;

  SDL_GLContext* create_context(SDL_Window* window);

  void _go();
  void _printTimings();

public:
  static Engine& singleton();

  void set_app(App& app);
  void go();
  void pause();
  void resume();
  void stop();

  void enableMultisample();
  void disableMultisample();

  void contextLost();
  void contextRestored();

  float lastFrameTime();
  void pushTimingNote(const std::string& string);
  void popTimingNote();
  void printNextTimings();
};
