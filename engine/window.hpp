#pragma once

#include <SDL_events.h>

#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>

struct SDL_Window;

class Engine;

class Window {
protected:
  char const*const title;
  int _width, _height;
  SDL_Window* window;
  SDL_GLContext* context;
  glm::mat4 matrix;
  friend class Engine;

public:
  Window(const char* title, int width=800, int height=600, int x=SDL_WINDOWPOS_UNDEFINED, int y=SDL_WINDOWPOS_UNDEFINED);
  ~Window();

  Window(const Window& copy) = delete;
  Window& operator=(const Window& copy) = delete;
  Window(Window&& move) = delete;
  Window& operator=(Window&& move) = delete;

  bool use();
  void swap();

  glm::mat4 pixel2device() const;
  glm::vec2 pixel2device(float x, float y) const;
  void contextRestored();
  int width() const;
  int height() const;
};
