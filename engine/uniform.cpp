#include "uniform.hpp"
#include "texture.hpp"
#include "shadervar.hpp"
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat2x2.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>


Uniform::Uniform() :
uniform_location(-1) {

}

Uniform::Uniform(const ShaderVar*const shaderVar) :
	Uniform(shaderVar->shader_program, shaderVar->name.c_str()) {

}

Uniform::Uniform(GLuint shader_program, const GLchar*const uniform_name) :
uniform_location(glGetUniformLocation(shader_program, uniform_name)) {

}

bool Uniform::isValid() const {
  return (uniform_location != -1);
}

void Uniform::float1(GLfloat x) {
  glUniform1f(uniform_location, x);
}

void Uniform::float2(GLfloat x, GLfloat y) {
  glUniform2f(uniform_location, x, y);
}

void Uniform::float3(GLfloat x, GLfloat y, GLfloat z) {
  glUniform3f(uniform_location, x, y, z);
}

void Uniform::float4(GLfloat x, GLfloat y, GLfloat z, GLfloat a) {
  glUniform4f(uniform_location, x, y, z, a);
}

void Uniform::vec2(const glm::vec2& vec) {
  glUniform2fv(uniform_location, 1, glm::value_ptr(vec));
}

void Uniform::vec3(const glm::vec3& vec) {
  glUniform3fv(uniform_location, 1, glm::value_ptr(vec));
}

void Uniform::vec4(const glm::vec4& vec) {
  glUniform4fv(uniform_location, 1, glm::value_ptr(vec));
}

void Uniform::matrix2(const glm::mat2& mat) {
  glUniformMatrix2fv(uniform_location, 1, GL_FALSE, glm::value_ptr(mat));
}

void Uniform::matrix3(const glm::mat3& mat) {
  glUniformMatrix3fv(uniform_location, 1, GL_FALSE, glm::value_ptr(mat));
}

void Uniform::matrix4(const glm::mat4& mat) {
  glUniformMatrix4fv(uniform_location, 1, GL_FALSE, glm::value_ptr(mat));
}

void Uniform::texture(const Texture& texture) {
  glUniform1i(uniform_location, texture.texture_unit);
}
