#include "fragmentshader.hpp"

FragmentShader::FragmentShader(const File& file) :
Shader(file, GL_FRAGMENT_SHADER) {

}

FragmentShader::FragmentShader(const std::string& source) :
Shader(source, GL_FRAGMENT_SHADER) {

}

