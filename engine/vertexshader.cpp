#include "vertexshader.hpp"

VertexShader::VertexShader(const File& file) :
	Shader(file, GL_VERTEX_SHADER) {

}

VertexShader::VertexShader(const std::string& source) :
	Shader(source, GL_VERTEX_SHADER) {

}