#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

#include <glm/mat2x2.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>

class Texture;
class ShaderVar;

class Uniform {
protected:
	GLint uniform_location;
public:
	Uniform();
	Uniform(const ShaderVar*const shaderVar);
	Uniform(GLuint program, const GLchar*const name);
	~Uniform() = default;

	Uniform(const Uniform& copy) = default;
	Uniform& operator=(const Uniform& copy) = default;

	Uniform(Uniform&& move) = default;
	Uniform& operator=(Uniform&& move) = default;

	bool isValid() const;

	void float1(GLfloat x);
	void float2(GLfloat x, GLfloat y);
	void float3(GLfloat x, GLfloat y, GLfloat z);
	void float4(GLfloat x, GLfloat y, GLfloat z, GLfloat a);
	void vec2(const glm::vec2& vec);
	void vec3(const glm::vec3& vec);
	void vec4(const glm::vec4& vec);
	void matrix2(const glm::mat2& mat);
	void matrix3(const glm::mat3& mat);
	void matrix4(const glm::mat4& mat);
	void texture(const Texture& texture);
};
