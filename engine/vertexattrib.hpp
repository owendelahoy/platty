#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

class VertexAttrib {
public:
  int slot;
  GLint size;
  GLenum type;
  GLboolean normalized;
  GLsizei stride;
  const GLvoid * pointer;

  VertexAttrib(int slot = 0, GLint size = 4, GLenum type = GL_FLOAT, GLboolean normalized = GL_FALSE, GLsizei stride = 0, const GLvoid* pointer = 0);
  ~VertexAttrib() = default;

  VertexAttrib(const VertexAttrib& copy) = default;
  VertexAttrib(VertexAttrib&& move) = default;
  VertexAttrib& operator=(const VertexAttrib& copy) = default;
  VertexAttrib& operator=(VertexAttrib&& move) = default;
};
