#include "file.hpp"

#include <iostream>
#include <fstream>

File::File(const std::string& filename) :
filename(filename),
_contents(0, ' '),
loaded(false) {
  load();
}

bool File::load() {
  std::ifstream file(filename, std::ios_base::in | std::ios_base::ate);
  if(file.is_open()) {
    auto size = file.tellg();
    std::cout << '\'' << filename << "' size: " <<  size << std::endl;
    _contents.resize(size);
    file.seekg(0);
    file.read(&(_contents[0]), size); //Compliant in C++11, but probably ok in older.
    if(!file.bad()) {
      return loaded=true;
    }
  }
  return loaded=false;
}

bool File::isLoaded() const {
  return loaded;
}

const std::string& File::contents() const {
  return _contents;
}
