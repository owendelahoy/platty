#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

class ShaderProgram;
class VertexBuffer;
class VertexAttrib;
class ShaderVar;

class VertexArray {
private:
	//These copy constructors invoked by the move constructor
	VertexArray(const VertexArray& copy) = default;
	VertexArray& operator=(const VertexArray& copy) & = default;
	void invalidate() noexcept;
protected:
	GLuint vao;
public:
	VertexArray();
	~VertexArray();

	VertexArray(VertexArray&& move) noexcept;
	VertexArray& operator=(VertexArray&& move) noexcept;

	void bind() const;
	void draw(GLenum mode, GLint first, GLsizei count) const;
	void drawElements(GLenum mode, GLsizei count) const;
	void attrib(GLuint index = 0, GLint size = 4, GLsizei stride = 0, const GLvoid * pointer = 0, GLenum type = GL_FLOAT, GLboolean normalized = GL_FALSE);
	void attrib(const ShaderVar*const attribute, GLsizei stride = 0, const GLvoid * pointer = 0, GLenum type = GL_FLOAT, GLboolean normalized = GL_FALSE);
	void enableAttrib(GLuint index);
	void disableAttrib(GLuint index);
};
