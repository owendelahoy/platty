#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

#include "shader.hpp"

class FragmentShader : public Shader {
public:
  FragmentShader(const File& File);
  FragmentShader(const std::string& source);

  FragmentShader(const FragmentShader& copy) = default;
  FragmentShader& operator=(const FragmentShader& copy) & = default;
  
  FragmentShader(FragmentShader&& move) = default;
  FragmentShader& operator=(FragmentShader&& move) & = default;
};
