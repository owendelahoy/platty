#include "texture.hpp"
#include <iostream>

void Texture::invalidate() noexcept {
  texture = 0;
  texture_unit = 0;
}

void Texture::image(const GLvoid* data, GLenum type, GLsizei width, GLsizei height) {
  bind();
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, type, data);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

void Texture::image(const unsigned char* data, GLsizei width, GLsizei height) {
  image(data, GL_UNSIGNED_BYTE, width, height);
}

void Texture::bind() {
  glBindTexture(GL_TEXTURE_2D, texture);
}

Texture::Texture() :
texture(0),
texture_unit(0) {
  glGenTextures(1, &texture);
  std::cout << "Texture(" << texture << ") created" << std::endl;
}

Texture::~Texture() {
  std::cout << "Texture(" << texture << ") destroyed" << std::endl;
  glDeleteTextures(1, &texture);
}

Texture::Texture(Texture&& move) noexcept :
Texture(static_cast<Texture&>(move)) {
  move.invalidate();
}

Texture& Texture::operator=(Texture&& move) noexcept {
  (*this) = static_cast<Texture&>(move);
  move.invalidate();
  return (*this);
}

void Texture::test() {
  const unsigned char pixels[] = {
    0xFF,0x0,0x0,0xFF,   0x0,0xFF,0x0,0xFF,
    0x0,0x0,0xFF,0xFF,   0xFF,0x0,0xFF,0xFF,
  };
  use(0);
  image(pixels, 2, 2);
}

bool Texture::use(int texture_unit) {
  if(texture_unit >= 0 && texture_unit < GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS) {
    this->texture_unit = texture_unit;
    bind();
    glActiveTexture(GL_TEXTURE0 + texture_unit);
    return true;
  }
  return false;
}
