#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

class Texture {
  friend class Uniform;
private:
  //These copy constructors invoked by the move constructor
  Texture(const Texture& copy) = default;
  Texture& operator=(const Texture& copy) = default;
  void invalidate() noexcept;

protected:
  GLuint texture;
  int texture_unit;

  void image(const GLvoid* data, GLenum type, GLsizei width, GLsizei height);
  void image(const unsigned char* data, GLsizei width, GLsizei height);
  void bind();
public:
  Texture();
  ~Texture();

  Texture(Texture&& move) noexcept;
  Texture& operator=(Texture&& move) noexcept;

  void test();
  bool use(int texture);
};
