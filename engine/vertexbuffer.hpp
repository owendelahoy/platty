#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

#include <vector>
#include "glm/fwd.hpp"

class VertexBuffer {
private:
	//These copy constructors invoked by the move constructor
	VertexBuffer(const VertexBuffer& copy) = default;
	VertexBuffer& operator=(const VertexBuffer& copy) = default;
	void invalidate() noexcept;
protected:
	GLuint vbo;
public:
	VertexBuffer();
	~VertexBuffer();

	VertexBuffer(VertexBuffer&& move) noexcept;
	VertexBuffer& operator=(VertexBuffer&& move) noexcept;

	void bind(GLenum target = GL_ARRAY_BUFFER) const;
	void data(GLsizei size, const GLvoid* data_pointer, GLenum usage = GL_STATIC_DRAW, GLenum target = GL_ARRAY_BUFFER);
	void sub_data(GLintptr offset, GLsizei size, const GLvoid* data_pointer, GLenum target = GL_ARRAY_BUFFER);

	template<typename T>
	void vertexData(std::vector<T> data, GLenum usage = GL_STATIC_DRAW) {
		vertexData(data.size() * sizeof(T), data.data(), usage);
	}
	void vertexData(GLsizei size, const GLvoid* data_pointer, GLenum usage = GL_STATIC_DRAW);

	template<typename T>
	void vertexDataNoBind(std::vector<T> data, GLenum usage = GL_STATIC_DRAW) {
		vertexDataNoBind(data.size() * sizeof(T), data.data(), usage);
	}
	void vertexDataNoBind(GLsizei size, const GLvoid* data_pointer, GLenum usage = GL_STATIC_DRAW);
	
	template<typename T>
	void indexData(std::vector<T> data, GLenum usage = GL_STATIC_DRAW) {
		indexData(data.size() * sizeof(T), data.data(), usage);
	}
	void indexData(GLsizei size, const GLvoid* data_pointer, GLenum usage = GL_STATIC_DRAW);
	
	template<typename T>
	void indexDataNoBind(std::vector<T> data, GLenum usage = GL_STATIC_DRAW) {
		indexDataNoBind(data.size() * sizeof(T), data.data(), usage)
	}
	void indexDataNoBind(GLsizei size, const GLvoid* data_pointer, GLenum usage = GL_STATIC_DRAW);
};