#include "shadervar.hpp"

ShaderVar::ShaderVar(glGetActiveVar getVar, GLuint shader_program, GLuint index, GLint name_max_length) :
	shader_program(shader_program),
	index(index),
	name(name_max_length, '\0'),
	size(-1),
	type(0) {
	GLsizei length = 0;
	getVar(shader_program, index, name_max_length, &length, &size, &type, &(name[0]));
	name.resize(length);

	switch (type) {
	case GL_FLOAT:
		break;
	case GL_FLOAT_VEC2:
		size *= 2;
		break;
	case GL_FLOAT_VEC3:
		size *= 3;
		break;
	case GL_FLOAT_VEC4:
	case GL_FLOAT_MAT2:
		size *= 4;
		break;
	case GL_FLOAT_MAT2x3:
	case GL_FLOAT_MAT3x2:
		size *= 6;
		break;
	case GL_FLOAT_MAT2x4:
	case GL_FLOAT_MAT4x2:
		size *= 8;
		break;
	case GL_FLOAT_MAT3:
		size *= 9;
		break;
	case GL_FLOAT_MAT3x4:
	case GL_FLOAT_MAT4x3:
		size *= 12;
		break;
	case GL_FLOAT_MAT4:
		size *= 16;
	}
}