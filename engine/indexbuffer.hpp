#pragma once

#include "vertexattrib.hpp"

#include "gsl.h"

#define GLEW_STATIC
#include <GL/glew.h>

class VertexAttrib;

class VertexBuffer {
private:
  //These copy constructors invoked by the move constructor
  VertexBuffer(const VertexBuffer& copy) = default;
  VertexBuffer& operator=(const VertexBuffer& copy) = default;
  void invalidate() noexcept;

protected:
  GLuint vbo;
  VertexAttrib* vertex_attrib;

public:
  VertexBuffer(VertexAttrib& vertex_attrib);
  ~VertexBuffer();

  VertexBuffer(VertexBuffer&& move) noexcept;
  VertexBuffer& operator=(VertexBuffer&& move) noexcept;

  void bind() const;
  const VertexAttrib& getVertexAttrib() const;

  void setVertexAttrib(int slot = 0, GLint size = 4, GLenum type = GL_FLOAT, GLboolean normalized = GL_FALSE, GLsizei stride = 0, const GLvoid* pointer = 0); //TODO: multi attrib VertexBuffers
  void data(gsl::span<GLfloat> data, GLenum usage = GL_STATIC_DRAW);
  void sub_data(const GLvoid*const data, GLintptr offset, GLsizeiptr size);
};
