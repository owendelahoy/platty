#pragma once

#include <string>

#define GLEW_STATIC
#include <GL/glew.h>

class ShaderProgram;
class File;

class Shader {
	friend class ShaderProgram;
private:
	Shader(const Shader& copy) = default;
	Shader& operator=(const Shader& copy) = default;
	void invalidate() noexcept;
protected:
	GLenum shader_type;
	GLuint shader; //Initialisation depends on shader_type
	GLint compile_status;

	bool buildShader(const std::string& source);
public:
	Shader(const File& file, GLenum shader_type);
	Shader(const std::string& source, GLenum shader_type);
	virtual ~Shader();

	Shader(Shader&& move) noexcept;
	Shader& operator=(Shader&& move) noexcept;

	bool isCompiled() const;
	GLenum shaderType() const;
};
