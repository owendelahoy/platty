#include "vertexattrib.hpp"

VertexAttrib::VertexAttrib(int slot, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer) :
slot(slot),
size(size),
type(type),
normalized(normalized),
stride(stride),
pointer(pointer) {

}