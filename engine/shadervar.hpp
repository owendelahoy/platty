#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

#include <string>

typedef void(*glGetActiveVar)(
	GLuint program,
	GLuint index,
	GLsizei bufSize,
	GLsizei* length,
	GLint* size,
	GLenum* type,
	GLchar* name);

class ShaderVar {
public:
	GLuint shader_program;
	GLuint index;
	std::string name;
	GLint size;
	GLenum type;

	ShaderVar(glGetActiveVar getVar, GLuint shader_program, GLuint index, GLint name_max_length = 1024);
	~ShaderVar() = default;

	ShaderVar(const ShaderVar& copy) = default;
	ShaderVar& operator=(const ShaderVar& copy) = default;
	ShaderVar(ShaderVar&& move) = default;
	ShaderVar& operator=(ShaderVar&& move) = default;
};