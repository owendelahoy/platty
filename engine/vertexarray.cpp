#include "vertexarray.hpp"

#include "shaderprogram.hpp"
#include "shadervar.hpp"
#include "vertexattrib.hpp"
#include "vertexbuffer.hpp"

#include <iostream>

void VertexArray::invalidate() noexcept {
  vao = 0;
}

VertexArray::VertexArray() :
vao(0) {
    glGenVertexArrays(1,&vao);
    printf("VertexArray(%i) created\n", vao);
}

VertexArray::~VertexArray() {
  glDeleteVertexArrays(1, &vao);
  printf("VertexArray(%i) destroyed\n", vao);
}

VertexArray::VertexArray(VertexArray&& move) noexcept :
VertexArray(static_cast<VertexArray&>(move)) {
  move.invalidate();
}

VertexArray& VertexArray::operator=(VertexArray&& move) noexcept {
  (*this) = static_cast<VertexArray&>(move);
  move.invalidate();
  return *this;
}

void VertexArray::bind() const {
  glBindVertexArray(vao);
}

void VertexArray::enableAttrib(GLuint index) {
  glEnableVertexAttribArray(index);
  std::cout << "attrib(" << index << ") enabled on VertexArray(" << vao << ")" << std::endl;
}

void VertexArray::disableAttrib(GLuint index) {
  glDisableVertexAttribArray(index);
}

void VertexArray::attrib(GLuint index, GLint size, GLsizei stride, const GLvoid* pointer, GLenum type, GLboolean normalized) {
	enableAttrib(index);
	glVertexAttribPointer(index, size, type, normalized, stride, pointer);
}

void VertexArray::attrib(const ShaderVar*const attribute, GLsizei stride, const GLvoid* pointer, GLenum type, GLboolean normalized) {
	attrib(attribute->index, attribute->size, stride, pointer, type, normalized);
}

void VertexArray::draw(GLenum mode, GLint first, GLsizei count) const {
	glDrawArrays(mode, first, count);
}

void VertexArray::drawElements(GLenum mode, GLsizei count) const {
	glDrawElements(mode, count, GL_UNSIGNED_SHORT, 0);
}
