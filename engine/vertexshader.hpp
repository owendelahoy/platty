#pragma once

#include "shader.hpp"

#include <string>

class File;

class VertexShader : public Shader {
public:
	VertexShader(const File& file);
	VertexShader(const std::string& source);

	~VertexShader() = default;
	VertexShader(const VertexShader& copy) = default;
	VertexShader& operator=(const VertexShader& copy) = default;
	VertexShader(VertexShader&& move) = default;
	VertexShader& operator=(VertexShader&& move) = default;
};
