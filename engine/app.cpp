#include "app.hpp"
#include "engine.hpp"

App::App() {
    Engine::singleton();
    std::cout << "App constructed" << std::endl;
}
