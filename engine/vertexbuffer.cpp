#include "vertexbuffer.hpp"

#include "glm/vec2.hpp"
#include "glm/vec3.hpp"
#include "glm/vec4.hpp"

#include <iostream>

void VertexBuffer::invalidate() noexcept {
  vbo = 0;
}

VertexBuffer::VertexBuffer() :
vbo(0) {
  glGenBuffers(1, &vbo);
  std::cout << "VertexBuffer(" << vbo << ") created" << std::endl;
}

VertexBuffer::~VertexBuffer() {
  glDeleteBuffers(1, &vbo);
  std::cout << "VertexBuffer(" << vbo << ") destroyed" << std::endl;
}

VertexBuffer::VertexBuffer(VertexBuffer&& move) noexcept :
VertexBuffer(static_cast<VertexBuffer&>(move)) {
	move.invalidate();
}

VertexBuffer& VertexBuffer::operator=(VertexBuffer&& move) noexcept {
	(*this) = static_cast<VertexBuffer&>(move);
	move.invalidate();
	return *this;
}

void VertexBuffer::bind(GLenum target) const {
  glBindBuffer(target, vbo);
  std::cout << "VertexBuffer(" << vbo << ") bound" << std::endl;
}

void VertexBuffer::data(GLsizei size, const GLvoid* data_pointer, GLenum usage, GLenum target) {
	glBufferData(target, size, data_pointer, usage);
	std::cout << size << " bytes bound to VertexBuffer(" << vbo << ")" << std::endl;
	const GLushort* a = reinterpret_cast<const GLushort*>(data_pointer);
	for (; size >= 0; size -= sizeof(GLushort)) {
		std::cout << a[size] << ", ";
	}
	std::cout << std::endl;
}

void VertexBuffer::sub_data(GLintptr offset, GLsizei size, const GLvoid* data, GLenum target) {
	glBufferSubData(target, offset, size, data);
	std::cout << size << " bytes modified in VertexBuffer(" << vbo << ") at offset " << offset << std::endl;
}

void VertexBuffer::vertexData(GLsizei size, const GLvoid* data_pointer, GLenum usage) {
	bind(GL_ARRAY_BUFFER);
	vertexDataNoBind(size, data_pointer, usage);
}

void VertexBuffer::vertexDataNoBind(GLsizei size, const GLvoid* data_pointer, GLenum usage) {
	data(size, data_pointer, usage, GL_ARRAY_BUFFER);
}

void VertexBuffer::indexData(GLsizei size, const GLvoid* data_pointer, GLenum usage) {
	bind(GL_ELEMENT_ARRAY_BUFFER);
	indexDataNoBind(size, data_pointer, usage);
}

void VertexBuffer::indexDataNoBind(GLsizei size, const GLvoid* data_pointer, GLenum usage) {
	data(size, data_pointer, usage, GL_ELEMENT_ARRAY_BUFFER);
}
