#include "shaderprogram.hpp"

#include "vertexshader.hpp"
#include "fragmentshader.hpp"
#include "shadervar.hpp"

#include <algorithm>
#include <iostream>

bool ShaderProgram::validShaders(const VertexShader& vertex_shader, const FragmentShader& fragment_shader) {
	return (vertex_shader.isCompiled()
		&& fragment_shader.isCompiled()
		&& fragment_shader.shaderType() == GL_FRAGMENT_SHADER);
}

class AttachShader {
public:
	const GLuint shader_program;
	const GLuint shader;

	AttachShader(const GLuint shader_program, const GLuint shader) :
		shader_program(shader_program),
		shader(shader) {
		glAttachShader(shader_program, shader);
		std::cout << "Shader(" << shader << ") Attached to ShaderProgram(" << shader_program << ")" << std::endl;
	}

	~AttachShader() {
		glDetachShader(shader_program, shader);
		std::cout << "Shader(" << shader << ") Dettached to ShaderProgram(" << shader_program << ")" << std::endl;
	}

	AttachShader(const AttachShader& copy) = delete;
	AttachShader& operator=(const AttachShader& copy) = delete;
	AttachShader(AttachShader&& move) = delete;
	AttachShader& operator=(AttachShader&& move) = delete;
};

bool ShaderProgram::buildShaderProgram(VertexShader& vertex_shader, FragmentShader& fragment_shader) {
	std::cout << "Building ShaderProgram(" << shader_program << ')' << std::endl;
	AttachShader vertex_attach{ shader_program, vertex_shader.shader };
	AttachShader fragment_attach{ shader_program, fragment_shader.shader };
	glLinkProgram(shader_program);
	glGetProgramiv(shader_program, GL_LINK_STATUS, &link_status);
	if (isLinked()) {
		std::cout << "ShaderProgram(" << shader_program << ") Linked" << std::endl;
		fetchAttributes();
		fetchUniforms();
		return true;
	}
	std::cout << "ShaderProgram(" << shader_program << ") failed to link" << std::endl;
	return false;
}

void ShaderProgram::fetchUniforms() {
	GLint uniform_count = 0;
	glGetProgramiv(shader_program, GL_ACTIVE_UNIFORMS, &uniform_count);
	GLint string_length = 0;
	glGetProgramiv(shader_program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &string_length);
	for (int index = 0; index < uniform_count; index++) {
		uniforms.emplace_back(glGetActiveUniform, shader_program, index, string_length);
	}
}

void ShaderProgram::fetchAttributes() {
	GLint attribute_count = 0;
	glGetProgramiv(shader_program, GL_ACTIVE_ATTRIBUTES, &attribute_count);
	GLint string_length = 0;
	glGetProgramiv(shader_program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &string_length);
	for (int index = 0; index < attribute_count; index++) {
		attributes.emplace_back(glGetActiveAttrib, shader_program, index, string_length);
	}
}

void ShaderProgram::invalidate() noexcept {
	shader_program = 0;
	link_status = GL_FALSE;
	uniforms.clear();
	attributes.clear();
}

ShaderProgram::ShaderProgram(const File& vs_file, const File& fs_file) :
	ShaderProgram(VertexShader(vs_file), FragmentShader(fs_file)) {

}

ShaderProgram::ShaderProgram(VertexShader&& vertex_shader, FragmentShader&& fragment_shader) :
	ShaderProgram::ShaderProgram(vertex_shader, fragment_shader) {

}

ShaderProgram::ShaderProgram(VertexShader& vertex_shader, FragmentShader& fragment_shader) :
	shader_program(glCreateProgram()),
	link_status(GL_FALSE),
	uniforms(),
	attributes() {
	printf("Shader program created\n");
	if (shader_program != 0 && validShaders(vertex_shader, fragment_shader)) {
		if (buildShaderProgram(vertex_shader, fragment_shader)) {
			std::cout << "Shader program(" << shader_program << ") built" << std::endl;
		}
		else {
			printf("Shader program(%i) could not be constructed.\n", shader_program);
		}
	}
	else {
		std::cout << "Shader program(" << shader_program << ") could not be built due to invalid shaders." << std::endl;
	}
}

ShaderProgram::~ShaderProgram() {
	glDeleteProgram(shader_program);
	std::cout << "Shader program(" << shader_program << ") destroyed" << std::endl;
}

ShaderProgram::ShaderProgram(ShaderProgram&& move) noexcept :
ShaderProgram(static_cast<ShaderProgram&>(move)) {
	move.invalidate();
}

ShaderProgram& ShaderProgram::operator=(ShaderProgram&& move) noexcept {
	(*this) = static_cast<ShaderProgram&>(move);
	move.invalidate();
	return *this;
}

bool ShaderProgram::isLinked() const {
	return link_status == GL_TRUE;
}

void ShaderProgram::use() const {
	if (shader_program != 0 && isLinked()) {
		glUseProgram(shader_program);
	}
}

const ShaderVar* ShaderProgram::findShaderVar(const std::vector<ShaderVar>& shaderVars, const std::string& name) {
	auto pred = [&](const ShaderVar& var) {
		return var.name.compare(name) == 0;
	};
	auto found = std::find_if(shaderVars.cbegin(), shaderVars.cend(), pred);
	if (found == shaderVars.cend()) {
		return nullptr;
	}
	return &(*found);
}

const ShaderVar* const ShaderProgram::getAttribute(const std::string& name) const {
	return findShaderVar(attributes, name);
}

const ShaderVar* const ShaderProgram::getUniform(const std::string& name) const {
	return findShaderVar(uniforms, name);
}
