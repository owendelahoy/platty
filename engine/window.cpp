#include "window.hpp"

#include "engine.hpp"

Window::Window(const char* title, int width, int height, int x, int y) :
title(title),
_width(width),
_height(height),
window(SDL_CreateWindow(title, y, x, _width, _height, SDL_WINDOW_OPENGL)),
context(Engine::singleton().create_context(window)),
matrix(
  2.0f / width, 0.0f, 0.0f, 0.0f,
  0.0f, -2.0f / height, 0.0f, 0.0f,
  0.0f, 0.0f, 1.0f, 0.0f,
  -1.0f, 1.0f, 0.0f, 1.0f
) {
  if(window == NULL) {
    std::cout << "Could not create window: " << SDL_GetError() << std::endl;
  } else {
    std::cout << "Window created '" << title << "' " << _width << 'x' << _height << std::endl;
    SDL_GetWindowSize(window, &_width, &_height);
  }
}

Window::~Window() {
  SDL_DestroyWindow(window);
  std::cout << "Window destroyed '" << title << "' " << _width << 'x' << _height << std::endl;
}

void Window::swap() {
  SDL_GL_SwapWindow(window);
}

bool Window::use() {
  if(SDL_GL_MakeCurrent(window, *context) == 0) {
    return true;
  }
  std::cout << "Could not use window '" << title << "' "
  << _width << 'x' << _height
  << ": " << SDL_GetError() << std::endl;

  return false;
}

glm::mat4 Window::pixel2device() const {
  return matrix;
}

glm::vec2 Window::pixel2device(float x, float y) const {
  glm::vec4 coords = matrix * glm::vec4(x, y, 0.0f, 0.0f);
  return glm::vec2(coords.x, coords.y);
}

void Window::contextRestored() {
  context = Engine::singleton().create_context(window);
  std::cout << "Window context recreated." << std::endl;
}

int Window::width() const {
  return _width;
}

int Window::height() const {
  return _height;
}
