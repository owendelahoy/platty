#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

#include <vector>
#include <string>

class VertexShader;
class FragmentShader;
class File;
class ShaderVar;

class ShaderProgram {
private:
	ShaderProgram(const ShaderProgram& copy) = default;
	ShaderProgram& operator=(const ShaderProgram& copy) = default;
	void invalidate() noexcept;
protected:
	GLuint shader_program;
	GLint link_status;
	std::vector<ShaderVar> uniforms;
	std::vector<ShaderVar> attributes;

	bool validShaders(const VertexShader& vertex_shader, const FragmentShader& fragment_shader);
	bool buildShaderProgram(VertexShader& vertex_shader, FragmentShader& fragment_shader);
	void fetchUniforms();
	void fetchAttributes();
	static const ShaderVar* findShaderVar(const std::vector<ShaderVar>& shaderVars, const std::string& name);
public:
	ShaderProgram(const File& vs_file, const File& fs_file);
	ShaderProgram(VertexShader&& vertex_shader, FragmentShader&& fragment_shader);
	ShaderProgram(VertexShader& vertex_shader, FragmentShader& fragment_shader);
	~ShaderProgram();

	ShaderProgram(ShaderProgram&& move) noexcept;
	ShaderProgram& operator=(ShaderProgram&& move) noexcept;

	bool isLinked() const;
	void use() const;

	const ShaderVar* const getAttribute(const std::string& name) const;
	const ShaderVar* const getUniform(const std::string& name) const;
};
