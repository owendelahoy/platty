#include "vertexbuffer.hpp"

#include "vertexattrib.hpp"

#include <iostream>

void VertexBuffer::invalidate() noexcept {
  vbo = 0;
}

VertexBuffer::VertexBuffer(VertexAttrib& vertex_attrib) :
vbo(0),
vertex_attrib(&vertex_attrib) {
  glGenBuffers(1, &vbo);
  std::cout << "VertexBuffer(" << vbo << ") created" << std::endl;
}

VertexBuffer::~VertexBuffer() {
  glDeleteBuffers(1, &vbo);
  std::cout << "VertexBuffer(" << vbo << ") destroyed" << std::endl;
}

VertexBuffer::VertexBuffer(VertexBuffer&& move) noexcept :
VertexBuffer(static_cast<VertexBuffer&>(move)) {
	move.invalidate();
}

VertexBuffer& VertexBuffer::operator=(VertexBuffer&& move) noexcept {
	(*this) = static_cast<VertexBuffer&>(move);
	move.invalidate();
	return *this;
}

void VertexBuffer::bind() const {
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  std::cout << "VertexBuffer(" << vbo << ") bound" << std::endl;
}

const VertexAttrib& VertexBuffer::getVertexAttrib() const {
  return *vertex_attrib;
}

void VertexBuffer::setVertexAttrib(int slot, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer) {
  vertex_attrib->slot = slot;
  vertex_attrib->size = size;
  vertex_attrib->type = type;
  vertex_attrib->normalized = normalized;
  vertex_attrib->stride = stride;
  vertex_attrib->pointer = pointer;
}

void VertexBuffer::data(gsl::span<GLfloat> data, GLenum usage) {
  bind();
  glBufferData(GL_ARRAY_BUFFER, data.length_bytes(), data.data(), usage);
  std::cout << data.length_bytes() << " bytes bound to VertexBuffer(" << vbo << ")" << std::endl;
}

void VertexBuffer::sub_data(const GLvoid*const data, GLintptr offset, GLsizeiptr size) {
  bind();
  glBufferSubData(GL_ARRAY_BUFFER, offset, size, data);
}
