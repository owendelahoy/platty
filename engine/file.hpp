#pragma once

#include <string>

class File {
  std::string _contents;
  bool loaded;
public:
  const std::string filename;
  File(const std::string& filename); 
  bool load();
  bool isLoaded() const;
  const std::string& contents() const;
};
