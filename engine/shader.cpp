#include "shader.hpp"

#include "file.hpp"

#include <iostream>

#ifndef __has_feature
	#define __has_feature(x) 0  // Compatibility with non-clang compilers.
#endif

void Shader::invalidate() noexcept {
  shader = 0;
  compile_status = GL_FALSE;
}

bool Shader::buildShader(const std::string& source) {
  const char* source_copy = source.c_str();
  glShaderSource(shader, 1, &source_copy, NULL);
  glCompileShader(shader);
  glGetShaderiv(shader, GL_COMPILE_STATUS, &compile_status);

  if(isCompiled()) {
    printf("Shader(%i) compiled\n", shader);
    return true;
  } else {
	#if  __has_feature(cxx_runtime_array)
		GLint length;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
		if(length == 0) {
		  printf("ERROR Shader log length is 0.\n");
		  length = 512; //some bad gl drivers return 0, so lets try and at least get something
		} else {
			length += 1;
		}
    #else
	  constexpr GLint length = 512;
    #endif
	GLchar buffer[length];
    glGetShaderInfoLog(shader, length, NULL, buffer);
    printf("Failed to compile shader(%i): %s\n", shader, buffer);
  }
  return false;
}

Shader::Shader(const File& file, GLenum shader_type) :
Shader(file.contents(), shader_type) {

}

Shader::Shader(const std::string& source, GLenum shader_type) :
shader_type(shader_type),
shader(glCreateShader(shader_type)),
compile_status(GL_FALSE) {
  if(shader != 0) {
    std::cout << "Shader created(" << shader << ")" << std::endl;
    buildShader(source);
  }
}

Shader::~Shader() {
  std::cout << "Shader(" << shader << ") destroyed" << std::endl;
  glDeleteShader(shader);
}

Shader::Shader(Shader&& move) noexcept :
Shader(static_cast<Shader&>(move)) {
  move.invalidate();
}

Shader& Shader::operator=(Shader&& move) noexcept {
  (*this) = static_cast<Shader&>(move);
  move.invalidate();
  return *this;
}

bool Shader::isCompiled() const {
  return (compile_status == GL_TRUE);
}

GLenum Shader::shaderType() const {
  return shader_type;
}
