#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/html5.h>
#endif

#include "engine.hpp"
#include <SDL.h>

#include "app.hpp"
#include "shaderprogram.hpp"
#include "vertexarray.hpp"
#include "vertexattrib.hpp"
#include "vertexbuffer.hpp"
#include "vertexshader.hpp"
#include "window.hpp"

#include <stack>

#ifdef __EMSCRIPTEN__
int emscripten_context_lost(int eventType, const void *reserved, void *userData) {
  std::cout << "eventType: " << eventType << std::endl;
  Engine::singleton().contextLost();
  return true;
}

int emscripten_context_restored(int eventType, const void *reserved, void *userData) {
  std::cout << "eventType: " << eventType << std::endl;
  Engine::singleton().contextRestored();
  return true;
}
#endif

Engine::Engine() :
	context(NULL),
	app(nullptr),
	lastTime(std::chrono::steady_clock::now()),
	extraTime(0),
	deltaTime(0),
	printTimings(false),
	noteStackSize(0),
	notes() {
  #ifdef __EMSCRIPTEN__
  emscripten_set_webglcontextlost_callback(0, 0, true, emscripten_context_lost);
  emscripten_set_webglcontextrestored_callback(0, 0, true, emscripten_context_restored);
  #endif

  if(SDL_Init(SDL_INIT_VIDEO)) {
    printf("SDL could not be initialized: %s\n", SDL_GetError());
  }

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

  printf("Engine created\n");
}

Engine::~Engine() {
  if(context != NULL) {
    SDL_GL_DeleteContext(context);
  }

  SDL_Quit();
  printf("Engine destroyed\n");
}

SDL_GLContext* Engine::create_context(SDL_Window* window) {
  if(window != NULL && context == NULL) {
    this->window = window;
    context = SDL_GL_CreateContext(window);
    if(context == NULL) {
      printf("Could not create render context: %s\n", SDL_GetError());
    } else {
      glewExperimental = GL_TRUE;
      GLenum err = glewInit();
      if(err != GLEW_OK) {
        printf("Glew could not be initialized: %s\n", glewGetErrorString(err));
      } else {
        printf("Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
      }
    }
  }
  return &context;
}

void Engine::set_app(App& app) {
  this->app = &app;
}

#ifdef __EMSCRIPTEN__
void go_emscripten() {
  Engine::singleton()._go();
}
#endif

void Engine::go() {
  keep_going = true;
  #ifdef __EMSCRIPTEN__
  emscripten_set_main_loop(go_emscripten,0,false);
  #else
  lastTime = std::chrono::steady_clock::now();
  while(app != nullptr && keep_going) {
    _go();
  }
  #endif
}

void Engine::_go() {
	pushTimingNote("Go");
	while (app != nullptr && SDL_PollEvent(&sdl_event)) {
		app->event(sdl_event);
	}
	
	auto curTime = std::chrono::steady_clock::now();
	deltaTime = curTime - lastTime;
	extraTime += deltaTime;
	lastTime = curTime;

	if(app != nullptr) {
		while (extraTime > frameTime) {
			extraTime -= frameTime;
			pushTimingNote("Frame");
			app->step();
			popTimingNote();
		}
		pushTimingNote("Render");
		app->render();
		popTimingNote();
	}

	popTimingNote();
	if (printTimings) {
		_printTimings();
	}
	notes.clear();
}

void Engine::pause() {
  if(keep_going) {
    #ifdef __EMSCRIPTEN__
    emscripten_pause_main_loop();
    #endif
    keep_going = false;
	extraTime += std::chrono::steady_clock::now() - lastTime;
  }
}

void Engine::resume() {
	if(!keep_going) {
		lastTime = std::chrono::steady_clock::now();
		keep_going = true;
		#ifdef __EMSCRIPTEN__
		emscripten_resume_main_loop();
		#endif
	}
}

void Engine::stop() {
  printf("stopping...\n");
  keep_going = false;
  #ifdef __EMSCRIPTEN__
  emscripten_force_exit(0);
  #else
  exit(0);
  #endif
}

Engine& Engine::singleton() {
  static Engine singleton;
  return singleton;
}

void Engine::enableMultisample() {
  glEnable(GL_MULTISAMPLE);
}

void Engine::disableMultisample() {
  glDisable(GL_MULTISAMPLE);
}

void Engine::contextLost() {
  pause();
  std::cout << "Context lost" << std::endl;

  if(context != NULL) {
    SDL_GL_DeleteContext(context);
    context = NULL;
  }

  if(app != nullptr) {
    app->contextLost();
  }
}

void Engine::contextRestored() {
	if (window != NULL) {
		context = SDL_GL_CreateContext(window);
	}

	if (app != nullptr) {
		app->contextRestored();
	}
	std::cout << "Context restored" << std::endl;
	resume();
}

float Engine::lastFrameTime() {
	return std::chrono::duration_cast<std::chrono::microseconds>(deltaTime).count() / 16666.666f;
}

void Engine::pushTimingNote(const std::string& string) {
	glFinish();
	noteStackSize++;
	notes.emplace_back(noteStackSize, string, std::chrono::steady_clock::now());
}

void Engine::popTimingNote() {
	glFinish();
	auto end = std::chrono::steady_clock::now();
	for (auto i = notes.rbegin(); i != notes.rend(); i++) {
		auto& lastNote = *i;
		if (lastNote.stack == noteStackSize) {
			lastNote.time = end - lastNote.start;
			noteStackSize--;
			return;
		}
	}
}

void Engine::printNextTimings() {
	printTimings = true;
}

void Engine::_printTimings() {
	auto stack = std::stack<TimingNote>();
	for (const auto& note : notes) {
		while (!stack.empty() && stack.top().stack >= note.stack) {
			stack.pop();
		}
		for (auto i = stack.size(); i > 0; i--) {
			std::cout << "|  ";
		}

		auto timeCount = (stack.empty() ? frameTime : stack.top().time).count();
		std::cout
			<< note.tag
			<< ": "
			<< note.time.count()
			<< "ns ("
			<< 100.0f * note.time.count() / timeCount
			<< "%)"
			<< std::endl;
		stack.push(note);
	}
	printTimings = false;
}