#pragma once

#include <SDL_events.h>

class App {
public:
  App();
  virtual ~App() = default;

  App(const App& copy) = delete;
  App& operator=(const App& copy) = delete;
  App(App&& move ) = delete;
  App& operator=(App&& move) = delete;

  virtual void event(const SDL_Event& event) = 0;
  virtual void step() = 0;
  virtual void render() = 0;
  virtual void contextLost() = 0;
  virtual void contextRestored() = 0;
};
