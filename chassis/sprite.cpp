#include "sprite.hpp"

#include "engine/shadervar.hpp"

Sprite::Sprite(const ShaderVar*const position, const ShaderVar*const texture) :
	Quad(position),
	texture_coords() {
	GLfloat tex[] = { 0.0f,0.0f, 0.0f,1.0f, 1.0f,0.0f, 1.0f,1.0f };
	texture_coords.vertexData(sizeof(tex), tex, GL_STATIC_DRAW);
	vertex_array.attrib(texture->index, 2, 0, 0, GL_FLOAT, GL_FALSE);
}
