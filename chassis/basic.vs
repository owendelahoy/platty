#version 100

attribute vec3 vertex;
uniform mat4 camera;
uniform mat4 matrix;

void main() {
  gl_Position = camera * matrix * vec4(vertex, 1.0);
}
