#pragma once

#include "engine/vertexarray.hpp"
#include "engine/vertexbuffer.hpp"

#include <string>

class ShaderVar;

class Quad {
protected:
  VertexArray vertex_array;
  VertexBuffer vertex_buffer;
public:
  Quad(const ShaderVar*const attribute, float x1 = 1.0f, float y1 = 1.0f,float x2 = -1.0f, float y2 = -1.0f);
  ~Quad() = default;

  Quad(const Quad& copy) = delete;
  Quad& operator=(const Quad& copy) = delete;

  Quad(Quad&& move) = default;
  Quad& operator=(Quad&& move) = default;

  void preDraw();
  void draw();
};
