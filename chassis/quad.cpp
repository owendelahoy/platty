#include "quad.hpp"
#include "engine/shadervar.hpp"

Quad::Quad(const ShaderVar*const attribute, float x1, float y1, float x2, float y2) :
	vertex_buffer(),
	vertex_array() {
	vertex_array.bind();
	GLfloat quad[] = { x1,y1, x1,y2, x2,y1, x2,y2 };
	vertex_buffer.vertexData(sizeof(quad), quad, GL_STATIC_DRAW);
	vertex_array.attrib(attribute->index, 2, 0, 0, GL_FLOAT, GL_FALSE);
}

void Quad::preDraw() {
	vertex_array.bind();
}

void Quad::draw() {
	vertex_array.draw(GL_TRIANGLE_STRIP, 0, 4);
}
