#include "tbb.hpp"
#include "chassis/aabb.hpp"

#include <glm/geometric.hpp>
#include <glm/gtx/norm.hpp>

TBB::TBB() :
axis1(1.0f, 0.0f),
axis2(0.0f, 1.0f),
position(0.0f, 0.0f) {

}

TBB::TBB(const glm::vec2& position, const glm::vec2& axis, float orthogonal) :
axis1(axis),
axis2(
  -axis.y * orthogonal / glm::length(axis),
  axis.x * orthogonal / glm::length(axis)),
position(position) {
  
}

TBB::TBB(const glm::vec2& position, const glm::vec2& axis1, const glm::vec2& axis2) :
axis1(axis1),
axis2(axis2),
position(position) {
  
}

TBB::TBB(const glm::vec2& position, const glm::vec2& dimension) :
	axis1(dimension.x, 0.0f),
	axis2(0.0f, dimension.y),
	position(position) {

}

bool TBB::intersects(const TBB& that) const {
	const glm::vec2 delta = position - that.position;

	auto test_axis = [=](const glm::vec2& axis) -> bool {
		glm::vec2 normal(-axis.y, axis.x);
		normal = glm::normalize(normal);
		return std::fabs(glm::dot(normal, delta)) >
			std::fabs(glm::dot(normal, axis1)) +
			std::fabs(glm::dot(normal, axis2)) +
			std::fabs(glm::dot(normal, that.axis1)) +
			std::fabs(glm::dot(normal, that.axis2));
	};

	if (test_axis(axis1)) {
		return false;
	}

	if (test_axis(axis2)) {
		return false;
	}

	if (test_axis(that.axis1)) {
		return false;
	}

	if (test_axis(that.axis2)) {
		return false;
	}

	return true;
}

bool TBB::intersects(glm::vec2 position, float radius) const {
	const glm::vec2 delta = this->position - position;

	auto test_axis = [=](const glm::vec2& axis) -> bool {
		glm::vec2 normal(-axis.y, axis.x);
		normal = glm::normalize(normal);
		return std::fabs(glm::dot(normal, delta)) >
			std::fabs(glm::dot(normal, axis1)) +
			std::fabs(glm::dot(normal, axis2)) +
			radius;
	};

	if (test_axis(axis1)) {
		return false;
	}

	if (test_axis(axis2)) {
		return false;
	}

	return true;
}

bool TBB::intersects(const AABB2d& that) const {
	const glm::vec2 delta = position - that.position;
	const auto otherAxis1 = glm::vec2(that.scale);
	const auto otherAxis2 = glm::vec2(-otherAxis1.y, otherAxis1.x);

	auto test_axis = [=](const glm::vec2& axis) -> bool {
		glm::vec2 normal(-axis.y, axis.x);
		normal = glm::normalize(normal);
		return std::fabs(glm::dot(normal, delta)) >
			std::fabs(glm::dot(normal, axis1)) +
			std::fabs(glm::dot(normal, axis2)) +
			std::fabs(glm::dot(normal, otherAxis1)) +
			std::fabs(glm::dot(normal, otherAxis2));
	};

	if (test_axis(axis1)) {
		return false;
	}

	if (test_axis(axis2)) {
		return false;
	}

	if (test_axis(otherAxis1)) {
		return false;
	}

	if (test_axis(otherAxis2)) {
		return false;
	}

	return true;
}

glm::mat4 TBB::matrix() const {
	glm::mat4 matrix(1.0f);
	matrix[0][0] = axis1.x;
	matrix[0][1] = axis1.y;
	matrix[1][0] = axis2.x;
	matrix[1][1] = axis2.y;
	matrix[3][0] = position.x;
	matrix[3][1] = position.y;
	return matrix;
}