#pragma once

#include "quad.hpp"

#include "engine/vertexbuffer.hpp"

class ShaderVar;

class Sprite : public Quad {
protected:
	VertexBuffer texture_coords;
public:
	Sprite(const ShaderVar*const position, const ShaderVar*const texture);
	~Sprite() = default;

	Sprite(const Sprite&) = delete;
	Sprite& operator=(const Sprite&) = delete;
	Sprite(Sprite&& move) = default;
	Sprite& operator=(Sprite&& move) = default;
};
