#pragma once

#include <glm/mat4x4.hpp>

class Window;

class Camera {
protected:
  float aspectX, aspectY;
  glm::mat4 _matrix, _inverse_matrix;
public:
  Camera(const glm::mat4& matrix);
  Camera();
  ~Camera() = default;
  
  Camera(const Camera& copy) = default;
  Camera& operator=(const Camera& copy) = default;

  Camera(Camera&& move) = default;
  Camera& operator=(Camera&& move) = default;

  virtual void setAspect(const Window& window, bool clip = true);
  glm::mat4 matrix() const;
  glm::mat4 inverse_matrix() const;
};
