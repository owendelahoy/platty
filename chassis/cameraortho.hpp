#pragma once

#include "camera.hpp"
#include "aabb.hpp"

class CameraOrtho : public Camera {
protected:
  AABB2d bounds;
public:
  CameraOrtho(const AABB2d& bounds = AABB2d(glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 1.0f)));
  ~CameraOrtho() = default;

  CameraOrtho(const CameraOrtho& copy) = default;
  CameraOrtho& operator=(const CameraOrtho& copy) = default;
  
  CameraOrtho(CameraOrtho&& move) = default;
  CameraOrtho& operator=(CameraOrtho&& move) = default;
  
  void setBounds(const AABB2d& bounds);
  const AABB2d& getBounds();
  void setCenter(const glm::vec2& pos, const glm::vec2& offsetScale);

  void setAspect(const Window& window, bool clip = true);
};
