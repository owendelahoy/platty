#pragma once

#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>

class AABB2d {
public:
  glm::vec2 position, scale;
  
  AABB2d(
    const glm::vec2& position = glm::vec2(0.0f, 0.0f),
    const glm::vec2& scale = glm::vec2(1.0f, 1.0f));
  
  AABB2d(const AABB2d& copy) = default;  
  AABB2d& operator=(const AABB2d& copy) = default;  

  AABB2d(AABB2d&& move) = default;  
  AABB2d& operator=(AABB2d&& move) = default;
  
  glm::mat4 orthogonalProjection(float scaleX = 1.0f, float scaleY = 1.0f) const;  
};
