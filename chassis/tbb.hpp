#pragma once

#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>

class AABB2d;

class TBB {
public:
  glm::vec2 axis1;
  glm::vec2 axis2;
  glm::vec2 position;

  TBB();
  TBB(
    const glm::vec2& position,
    const glm::vec2& axis,
    float orthogonal);
  TBB(
    const glm::vec2& position,
    const glm::vec2& axis1,
    const glm::vec2& axis2);
  TBB(const glm::vec2& position, const glm::vec2& dimension);
  
  TBB(const TBB& copy) = default;
  TBB& operator=(const TBB& copy) = default;
  TBB(TBB&& move) = default;
  TBB& operator=(TBB&& move) = default;

  bool intersects(const TBB& that) const;
  bool intersects(glm::vec2 point, float radius) const;
  bool intersects(const AABB2d& that) const;

  glm::mat4 matrix() const;
};
