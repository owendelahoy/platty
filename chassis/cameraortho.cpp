#include "cameraortho.hpp"

#include <glm/matrix.hpp>

CameraOrtho::CameraOrtho(const AABB2d& bounds) :
Camera(bounds.orthogonalProjection()),
bounds(bounds) {

}

void CameraOrtho::setBounds(const AABB2d& bounds) {
  this->bounds = bounds;
  _matrix = this->bounds.orthogonalProjection(aspectY, aspectX);//Bugfix this 
  _inverse_matrix = glm::inverse(_matrix);
}

const AABB2d& CameraOrtho::getBounds() {
	return bounds;
}

void CameraOrtho::setCenter(const glm::vec2& pos, const glm::vec2& offsetScale) {
  bounds.position.x = -pos.x + offsetScale.x * bounds.scale.x;
  bounds.position.y = -pos.y + offsetScale.y * bounds.scale.y;
  _matrix = bounds.orthogonalProjection(aspectY, aspectX); //Bugfix this
  _inverse_matrix = glm::inverse(_matrix);
}

void CameraOrtho::setAspect(const Window& window, bool clip) {
  Camera::setAspect(window, clip);
  _matrix = bounds.orthogonalProjection(aspectY, aspectX); //Bugfix this
  _inverse_matrix = glm::inverse(_matrix);
}
