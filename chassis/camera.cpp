#include "camera.hpp"

#include "../engine/window.hpp"

#include <glm/matrix.hpp>

Camera::Camera() :
aspectX(1.0f),
aspectY(1.0f),
_matrix(1.0f),
_inverse_matrix(1.0f) {
  
}

Camera::Camera(const glm::mat4& matrix) :
aspectX(1.0f),
aspectY(1.0f),
_matrix(matrix),
_inverse_matrix(glm::inverse(matrix)) {
  
}

void Camera::setAspect(const Window& window, bool clip) {
  if((window.width() < window.height()) ^ clip) {
    aspectX = float(window.width()) / float(window.height());
    aspectY = 1.0f;
  } else {
    aspectX = 1.0f;
    aspectY = float(window.height()) /  float(window.width());
  }
}

glm::mat4 Camera::matrix() const {
  return _matrix;
}

glm::mat4 Camera::inverse_matrix() const {
  return _inverse_matrix;
}
