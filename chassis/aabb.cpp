#include "aabb.hpp"

AABB2d::AABB2d(const glm::vec2& position, const glm::vec2& scale) : 
position(position),
scale(scale) {

}

glm::mat4 AABB2d::orthogonalProjection(float aspectX, float aspectY) const {
  glm::mat4 matrix(1.0f);
  matrix[0][0] = aspectX / scale.x;
  matrix[1][1] = aspectY / scale.y;
  matrix[3][0] = position.x * matrix[0][0];
  matrix[3][1] = position.y * matrix[1][1];
  return matrix;
}
