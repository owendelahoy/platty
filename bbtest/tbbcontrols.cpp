#include "tbbcontrols.hpp"

#include <glm/geometric.hpp>

#include <algorithm>
#include <iostream>
TBBControls::TBBControls() :
tbb(),
select() {
  
}

TBBControls::TBBControls(const TBB& tbb) :
tbb(tbb),
select() {
  
}

void TBBControls::pointerDown(SDL_FingerID id, glm::vec2 pointer) {
  pointer  -= tbb.position;
  float delta1 = glm::dot(tbb.axis1, pointer) / glm::length(tbb.axis1);
  float delta2 = glm::dot(tbb.axis2, pointer) / glm::length(tbb.axis2);

  if(std::all_of(select.cbegin(), select.cend(), [&](auto pair){return &(pair.second) != &(tbb.position);})) {
    if(std::fabs(delta1) < glm::length(tbb.axis1) && std::fabs(delta2) < glm::length(tbb.axis2)) {
      select.insert({id, tbb.position});
    }
    std::cout << "D1: " << delta1 << " D2: " << delta2 << std::endl;
  } else {
    if(std::fabs(delta1) > std::fabs(delta2)) {
      select.insert({id, tbb.axis1});
    } else {
      select.insert({id, tbb.axis2});
    }
  }
}

void TBBControls::pointerUp(SDL_FingerID id) {
  if(select.erase(id) > 0) {
  }
}
void TBBControls::pointerMove(SDL_FingerID id, glm::vec2 delta) {
  auto find = select.find(id);
  if(find != select.end()) {
    find->second.x += delta.x;
    find->second.y += delta.y;
  }
}
