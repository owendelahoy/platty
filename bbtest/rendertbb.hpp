#pragma once

#include "engine/shaderprogram.hpp"
#include "engine/uniform.hpp"

#include "chassis/quad.hpp"

#include <glm/vec4.hpp>

class TBB;
class TBBControls;
class Camera;

class RenderTBB {
private:
  ShaderProgram shader_program;
  Uniform uniform_camera;
  Uniform uniform_matrix;
  Uniform uniform_color;
  Quad quad;

public:
  RenderTBB();
  void draw(const TBB& tbb, Camera& camera, glm::vec4 color = glm::vec4(1.0f));
  void draw(const TBBControls& tbbc, Camera& camera, glm::vec4 color = glm::vec4(1.0f));
};
