#include "chassis/tbb.hpp"

#include <SDL_events.h>
#include <map>

class TBBControls {
public:
  TBB tbb;
  std::map<SDL_FingerID, glm::vec2&> select;
  
  TBBControls();
  TBBControls(const TBB& tbb);
  ~TBBControls() = default;

  TBBControls(const TBBControls& copy) = default;
  TBBControls(TBBControls&& move) = default;
  
  TBBControls& operator=(const TBBControls& copy) = default;
  TBBControls& operator=(TBBControls&& move) = default;

  void pointerDown(SDL_FingerID id, glm::vec2 pointer);
  void pointerUp(SDL_FingerID id);
  void pointerMove(SDL_FingerID id, glm::vec2 delta);
};
