#include "bbtest.hpp"

#include "engine/all.hpp"

#include <glm/gtx/string_cast.hpp>

BBTest::BBTest() :
window("TBB test", 800, 450),
camera(AABB2d(glm::vec2(0.0f, 0.0f), glm::vec2(1024.0f, 1024.0f))),
tbb1(TBB(glm::vec2(-512.0f, 0.0f), glm::vec2(256.0f, -256.0f), glm::vec2(256.0f, 256.0f))),
tbb2(TBB(glm::vec2(512.0f, 0.0f), glm::vec2(256.0f, 0.0f), glm::vec2(0.0f, 256.0f))),
render_tbb(),
pointer(0.0f, 0.0f) {
  camera.setAspect(window, false);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  std::cout << glm::to_string(camera.matrix()) << std::endl;
  glClearColor(0.1f, 0.1f, 0.25f, 1.0f);
}

void BBTest::touchEvent(const SDL_TouchFingerEvent& event) {
  if(event.type == SDL_FINGERUP) {
    tbb1.pointerUp(event.fingerId);
    tbb2.pointerUp(event.fingerId);
  } else {
    glm::vec4 pointer;
    if(event.type == SDL_FINGERDOWN) {
      pointer = glm::vec4(event.x, event.y, 0.0f, 1.0f);
    } else {
      pointer = glm::vec4(event.dx, event.dy, 0.0f, 0.0f);
    }
	#ifndef EMSCRIPTEN
		pointer.x *= window.width();
		pointer.y *= window.height();
	#endif
    pointer = camera.inverse_matrix() * window.pixel2device() * pointer;
    glm::vec2 world(pointer.x, pointer.y);
    if(event.type == SDL_FINGERDOWN) {
      std::cout << glm::to_string(world) << std::endl;
      tbb1.pointerDown(event.fingerId, world);
      tbb2.pointerDown(event.fingerId, world);
    } else {
      tbb1.pointerMove(event.fingerId, world);
      tbb2.pointerMove(event.fingerId, world);
    } 
  }
}

void BBTest::event(const SDL_Event& event) {
  switch(event.type) {
  case SDL_QUIT:
    Engine::singleton().stop();
    break;
  case SDL_KEYDOWN:
    switch(event.key.keysym.sym) {
    case SDLK_ESCAPE:
      Engine::singleton().stop();
      break;
    }
    break;
  case SDL_KEYUP:
    break;
  case SDL_MOUSEBUTTONUP:
    break;
  case SDL_MOUSEBUTTONDOWN:
    break;
  case SDL_MOUSEMOTION:
    break;
  case SDL_FINGERUP:  
  case SDL_FINGERDOWN:
  case SDL_FINGERMOTION:
    touchEvent(event.tfinger);
    break;
  }
}

void BBTest::step() {

}

void BBTest::render() {
  glClear(GL_COLOR_BUFFER_BIT);
  
  glm::vec4 color1 = !tbb1.tbb.intersects(tbb2.tbb) ? glm::vec4(0.8f, 0.4f, 0.4f, 0.4f) : glm::vec4(0.4f, 0.8f, 0.4f, 0.4f);
  render_tbb.draw(tbb1, camera, color1);

  glm::vec4 color2 = !tbb2.tbb.intersects(tbb1.tbb) ? glm::vec4(0.8f, 0.4f, 0.4f, 0.4f) : glm::vec4(0.4f, 0.8f, 0.4f, 0.4f);
  render_tbb.draw(tbb2, camera, color2);
  
  window.swap();
}

void BBTest::contextLost() {

}

void BBTest::contextRestored() {
	render_tbb = RenderTBB();
  
}
