#include "engine/engine.hpp"

#include "bbtest.hpp"

Engine& engine = Engine::singleton();

std::unique_ptr<BBTest> app; //TODO: move to stack, when emscripten bug is fixed

int main() {
  std::cout << "Begin main" << std::endl;
  app = std::make_unique<BBTest>();
  engine.enableMultisample();
  engine.set_app(*app);
  engine.go();
}
