#include "rendertbb.hpp"
#include "tbbcontrols.hpp"

#include "engine/file.hpp"
#include "chassis/camera.hpp"

RenderTBB::RenderTBB() :
shader_program(File("shaders/basic.vs"), File("shaders/color.fs")),
uniform_camera(shader_program.getUniform("camera")),
uniform_matrix(shader_program.getUniform("matrix")),
uniform_color(shader_program.getUniform("color")),
quad(shader_program.getAttribute("vertex")) {
  
}

void RenderTBB::draw(const TBB& tbb, Camera& camera, glm::vec4 color) {
  shader_program.use();
  glm::mat4 matrix; 
  matrix[0][0] = tbb.axis1.x;
  matrix[0][1] = tbb.axis1.y;
  matrix[1][0] = tbb.axis2.x;
  matrix[1][1] = tbb.axis2.y;
  matrix[3][0] = tbb.position.x;
  matrix[3][1] = tbb.position.y;
  uniform_matrix.matrix4(matrix);
  uniform_camera.matrix4(camera.matrix());
  uniform_color.vec4(color);
  quad.draw();
}


void RenderTBB::draw(const TBBControls& tbbc, Camera& camera, glm::vec4 color) {
  draw(tbbc.tbb, camera, color);
}
