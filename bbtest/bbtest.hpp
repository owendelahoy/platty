#pragma once
#include "tbbcontrols.hpp"
#include "rendertbb.hpp"

#include "engine/app.hpp"
#include "engine/window.hpp"

#include "chassis/cameraortho.hpp"

class BBTest : public App {
protected:
  Window window;
  CameraOrtho camera;
  
  TBBControls tbb1, tbb2;
  RenderTBB render_tbb;
  
  glm::vec2 pointer;
  void touchEvent(const SDL_TouchFingerEvent& event);
public:
  BBTest();
  ~BBTest() = default;
  
  BBTest(const BBTest& copy) = default;
  BBTest& operator=(const BBTest& copy) = default;

  BBTest(BBTest&& move) = default;
  BBTest& operator=(BBTest&& move) = default;
  
  void event(const SDL_Event& event) override;
  void step() override;
  void render() override;

  void contextLost() override;
  void contextRestored() override;
};
