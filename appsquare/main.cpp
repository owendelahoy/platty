#include "engine/engine.hpp"

#include "appsquare.hpp"

Engine& engine = Engine::singleton();

std::unique_ptr<AppSquare> app; //TODO: move to stack, when emscripten bug is fixed

int main() {
  std::cout << "Begin main" << std::endl;
  app = std::make_unique<AppSquare>();
  engine.enableMultisample();
  engine.set_app(*app);
  engine.go();
}
