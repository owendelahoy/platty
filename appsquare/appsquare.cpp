#include "appsquare.hpp"

#include "engine/engine.hpp"
#include "engine/vertexshader.hpp"
#include "engine/fragmentshader.hpp"

const char*const AppSquare::vertexShaderSource =
  "#version 100\n"
  "attribute vec2 vertexPosition;"
  "void main() {"
  "   gl_Position = vec4(vertexPosition, 0.0, 1.0);"
  "}";

const char*const AppSquare::fragmentShaderSource =
  "#version 100\n"
  "precision highp float;"
  "void main() {"
  "   gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);"
  "}";

AppSquare::AppSquare() :
	window("Quad Test"),
	shader_program(
		VertexShader(vertexShaderSource),
		FragmentShader(fragmentShaderSource)),
	vertex_buffer(),
	vertex_array(),
	press(false) {
	GLfloat vertices[] = { 0.5f, 0.5f, 0.5f, -0.5f, -0.5f, 0.5f, -0.5f, -0.5f };
	vertex_buffer.vertexData(sizeof(vertices),vertices);
	shader_program.use();
	vertex_array.attrib(shader_program.getAttribute("vertexPosition"));
}

void AppSquare::event(const SDL_Event& event) {
  if (event.type == SDL_QUIT)
  {Engine::singleton().stop();}
  else if (event.type == SDL_KEYDOWN) {
    if(event.key.keysym.sym == SDLK_ESCAPE)
    {Engine::singleton().stop();}
    if(event.key.keysym.sym == SDLK_c)
    {press = true;}
    if(event.key.keysym.sym == SDLK_r)
    {contextRestored();}
  } else if(event.type == SDL_KEYUP) {
    if(event.key.keysym.sym == SDLK_c)
    {press = false;}
  }
}

void AppSquare::step() {

}

void AppSquare::render() {
	glClearColor(press ? 0.0f : 0.5f, press ? 0.5f : 0.0f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	vertex_array.draw(GL_TRIANGLE_STRIP, 0, 4);
	window.swap();
}

void AppSquare::contextLost() {
}

void AppSquare::contextRestored() {
	// window.contextRestored();
	// window.use();
	VertexShader vertex_shader(vertexShaderSource);
	FragmentShader fragment_shader(fragmentShaderSource);
	shader_program = ShaderProgram(vertex_shader, fragment_shader);
	vertex_buffer = VertexBuffer();
	vertex_array = VertexArray();
	std::cout << "Objects reconstructed" << std::endl;

	GLfloat vertices[] = { 0.5f, 0.5f, 0.5f, -0.5f, -0.5f, 0.5f, -0.5f, -0.5f };
	vertex_buffer.vertexDataNoBind(sizeof(vertices), vertices);
	shader_program.use();
	vertex_array.bind();
	vertex_array.attrib(shader_program.getAttribute("vertexPosition"));
}
