#pragma once

#include "engine/app.hpp"
#include "engine/window.hpp"
#include "engine/shaderprogram.hpp"
#include "engine/vertexarray.hpp"
#include "engine/vertexbuffer.hpp"

#include "SDL_events.h"

class AppSquare : public App {
private:
  static const char*const vertexShaderSource;
  static const char*const fragmentShaderSource;

  Window window;
  ShaderProgram shader_program;
  VertexBuffer vertex_buffer;
  VertexArray vertex_array; //Depends on vertex_buffer
  bool press;
public:
  AppSquare();
  ~AppSquare() = default;

  void event(const SDL_Event& event) override;
  void step() override;
  void render() override;

  void contextLost() override;
  void contextRestored() override;
};
